ARG ARG_WORK_DIR=/build
ARG ARG_BUILD_CONF=compose

FROM node:16.14 as builder

ARG ARG_WORK_DIR
ARG ARG_BUILD_CONF

ENV TZ=Europe/Amsterdam
ENV PATH ${ARG_WORK_DIR}/node_modules/.bin:$PATH

RUN mkdir ${ARG_WORK_DIR}
WORKDIR ${ARG_WORK_DIR}

COPY package.json ${ARG_WORK_DIR}
COPY package-lock.json ${ARG_WORK_DIR}

RUN npm install

COPY . ${ARG_WORK_DIR}

RUN ng build --configuration=${ARG_BUILD_CONF}


FROM nginx:latest

ARG ARG_WORK_DIR

COPY  --from=builder ${ARG_WORK_DIR}/dist/poc-vng /usr/share/nginx/html

COPY ./resources/nginx/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080

CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.sample.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]
