import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbeidsmarktkwalificatieMatchKwalificatieComponent } from './arbeidsmarktkwalificatie-match-kwalificatie.component';

describe('ArbeidsmarktkwalificatieMatchKwalificatieComponent', () => {
  let component: ArbeidsmarktkwalificatieMatchKwalificatieComponent;
  let fixture: ComponentFixture<ArbeidsmarktkwalificatieMatchKwalificatieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArbeidsmarktkwalificatieMatchKwalificatieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbeidsmarktkwalificatieMatchKwalificatieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
