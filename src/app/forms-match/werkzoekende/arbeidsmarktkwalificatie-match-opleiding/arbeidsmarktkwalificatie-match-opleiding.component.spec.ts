import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbeidsmarktkwalificatieMatchOpleidingComponent } from './arbeidsmarktkwalificatie-match-opleiding.component';

describe('ArbeidsmarktkwalificatieMatchOpleidingComponent', () => {
  let component: ArbeidsmarktkwalificatieMatchOpleidingComponent;
  let fixture: ComponentFixture<ArbeidsmarktkwalificatieMatchOpleidingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArbeidsmarktkwalificatieMatchOpleidingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbeidsmarktkwalificatieMatchOpleidingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
