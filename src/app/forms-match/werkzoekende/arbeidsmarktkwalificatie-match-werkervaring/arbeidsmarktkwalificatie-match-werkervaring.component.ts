import {Component, Input, OnInit} from '@angular/core';
import {MPWerkzoekendeMatchBemiddelaar} from "../../../entities/werkzoekende/mPWerkzoekendeMatchBemiddelaar";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {selectToTextConvert} from "../../../forms-util/select-to-text";
import {codeWerkEnDenkniveauMinimaalValuesInteger} from "../../../forms-util/formcontrol-value-enums";

@Component({
  selector: 'app-arbeidsmarktkwalificatie-match-werkervaring',
  templateUrl: './arbeidsmarktkwalificatie-match-werkervaring.component.html',
  styleUrls: ['./arbeidsmarktkwalificatie-match-werkervaring.component.scss']
})
export class ArbeidsmarktkwalificatieMatchWerkervaringComponent implements OnInit {

  @Input()
  data?: MPWerkzoekendeMatchBemiddelaar; //TODO: FIX this, it is being used by vacature but doesnt work in OpleidingOngecodeerd

  arbeidsmarktkwalificatie?: MPArbeidsmarktkwalificatie;

  codeWerkEnDenkniveauWerkzoekende?: string | number

  constructor() {
  }

  ngOnInit(): void {
    this.codeWerkEnDenkniveauWerkzoekende = selectToTextConvert(this.data?.arbeidsmarktkwalificatie?.codeWerkEnDenkniveauWerkzoekende, codeWerkEnDenkniveauMinimaalValuesInteger)
    this.arbeidsmarktkwalificatie = this.data?.arbeidsmarktkwalificatie;
  }
}
