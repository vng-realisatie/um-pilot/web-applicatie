import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbeidsmarktkwalificatieMatchWerkervaringComponent } from './arbeidsmarktkwalificatie-match-werkervaring.component';

describe('ArbeidsmarktkwalificatieMatchWerkervaringComponent', () => {
  let component: ArbeidsmarktkwalificatieMatchWerkervaringComponent;
  let fixture: ComponentFixture<ArbeidsmarktkwalificatieMatchWerkervaringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArbeidsmarktkwalificatieMatchWerkervaringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbeidsmarktkwalificatieMatchWerkervaringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
