import {ModelDate} from "../entities/shared/modelDate";
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

export function convertToDateStruct(dateString: ModelDate | undefined): NgbDateStruct | null {
  if (!dateString) {
    return null;
  }

  const dateParts = dateString.trim().split('-');
  if (dateParts.length !== 3) {
    return null;
  }

  const year = parseInt(dateParts[0], 10);
  const month = parseInt(dateParts[1], 10);
  const day = parseInt(dateParts[2], 10);

  if (isNaN(year) || isNaN(month) || isNaN(day)) {
    return null;
  }

  return { year, month, day };
 }
