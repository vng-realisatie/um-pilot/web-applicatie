import {Component, Input, OnInit} from '@angular/core';
import {Werkzoekende} from "../../../entities/werkzoekende/werkzoekende";
import {Arbeidsmarktkwalificatie} from "../../../entities/werkzoekende/arbeidsmarktkwalificatie";
import {selectToTextConvert} from "../../../forms-util/select-to-text";
import {codeWerkEnDenkniveauMinimaalValuesInteger} from "../../../forms-util/formcontrol-value-enums";

@Component({
  selector: 'app-arbeidsmarktkwalificatie-detailed-werkervaring',
  templateUrl: './arbeidsmarktkwalificatie-detailed-werkervaring.component.html',
  styleUrls: ['./arbeidsmarktkwalificatie-detailed-werkervaring.component.scss']
})
export class ArbeidsmarktkwalificatieDetailedWerkervaringComponent implements OnInit {

  @Input()
  data?: Werkzoekende

  arbeidsmarktkwalificatie?: Arbeidsmarktkwalificatie;

  codeWerkEnDenkniveauWerkzoekende?: string | number;


  constructor() {
  }

  ngOnInit(): void {
    this.codeWerkEnDenkniveauWerkzoekende = selectToTextConvert(this.data?.arbeidsmarktkwalificatie?.codeWerkEnDenkniveauWerkzoekende, codeWerkEnDenkniveauMinimaalValuesInteger);
    this.arbeidsmarktkwalificatie = this.data?.arbeidsmarktkwalificatie;
  }

}

