import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ArbeidsmarktkwalificatieDetailedWerkervaringComponent} from './arbeidsmarktkwalificatie-detailed-werkervaring';

describe('ArbeidsmarktkwalificatieDetailedComponent', () => {
  let component: ArbeidsmarktkwalificatieDetailedWerkervaringComponent;
  let fixture: ComponentFixture<ArbeidsmarktkwalificatieDetailedWerkervaringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ArbeidsmarktkwalificatieDetailedWerkervaringComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbeidsmarktkwalificatieDetailedWerkervaringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
