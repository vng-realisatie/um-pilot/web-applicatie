import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbeidsmarktkwalificatieDetailedKwalificatieComponent } from './arbeidsmarktkwalificatie-detailed-kwalificatie.component';

describe('ArbeidsmarktkwalificatieDetailedKwalificatieComponent', () => {
  let component: ArbeidsmarktkwalificatieDetailedKwalificatieComponent;
  let fixture: ComponentFixture<ArbeidsmarktkwalificatieDetailedKwalificatieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArbeidsmarktkwalificatieDetailedKwalificatieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbeidsmarktkwalificatieDetailedKwalificatieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
