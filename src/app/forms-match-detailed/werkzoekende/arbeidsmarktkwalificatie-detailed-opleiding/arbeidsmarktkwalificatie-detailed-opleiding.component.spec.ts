import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArbeidsmarktkwalificatieDetailedOpleidingComponent } from './arbeidsmarktkwalificatie-detailed-opleiding.component';

describe('ArbeidsmarktkwalificatieDetailedOpleidingComponent', () => {
  let component: ArbeidsmarktkwalificatieDetailedOpleidingComponent;
  let fixture: ComponentFixture<ArbeidsmarktkwalificatieDetailedOpleidingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArbeidsmarktkwalificatieDetailedOpleidingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArbeidsmarktkwalificatieDetailedOpleidingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
