import {OpleidingsnaamOngecodeerdImpl} from "./opleidingsnaamOngecodeerdImpl";

export interface OpleidingsnaamOngecodeerd {
  opleidingsnaamOngecodeerd?: OpleidingsnaamOngecodeerdImpl
}
