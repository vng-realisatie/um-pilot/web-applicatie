import {OpleidingsnaamGecodeerdImpl} from "./opleidingsnaamGecodeerdImpl";

export interface OpleidingsnaamGecodeerd {
  opleidingsnaamGecodeerd?: OpleidingsnaamGecodeerdImpl
}
