export interface OpleidingsnaamOngecodeerdImpl {
  naamOpleidingOngecodeerd?: string;
  omschrijvingOpleiding?: string;
}
