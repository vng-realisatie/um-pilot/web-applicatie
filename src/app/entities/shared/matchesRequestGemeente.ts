import {Postcode} from "./postcode";
import {MPVacature} from "../vacature/mPVacature";
import {MPWerkzoekende} from "../werkzoekende/mPWerkzoekende";

export interface MatchesRequestGemeente {
  aanvraagKenmerk?: string,
  vraagObject: MPVacature | MPWerkzoekende
}
