import {BeroepsnaamOngecodeerdImpl} from "./beroepsnaamOngecodeerdImpl";

export interface BeroepsnaamOngecodeerd {
  beroepsnaamOngecodeerd?: BeroepsnaamOngecodeerdImpl
}
