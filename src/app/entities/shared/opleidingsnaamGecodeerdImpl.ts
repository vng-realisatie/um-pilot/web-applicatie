export interface OpleidingsnaamGecodeerdImpl {
  codeOpleidingsnaam?: number;
  omschrijvingOpleidingsnaam?: string;
}
