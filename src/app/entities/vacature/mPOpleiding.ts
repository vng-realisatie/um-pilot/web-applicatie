import {Opleidingsnaam} from "../shared/opleidingsnaam";


export interface MPOpleiding {
  codeNiveauOpleiding?: number
  indicatieDiploma?: number;
  opleidingsnaam?: Opleidingsnaam;
}
