import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {rijbewijsValues} from "../../../forms-util/formcontrol-value-enums";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";

@Component({
  selector: 'app-rijbewijs-form',
  templateUrl: './rijbewijs-form.component.html',
  styleUrls: ['./rijbewijs-form.component.css']
})
export class RijbewijsFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPArbeidsmarktkwalificatie>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  rijbewijsValues = rijbewijsValues;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {
    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.arbeidsmarktkwalificatie?.rijbewijs) {
        this.vraagObject.arbeidsmarktkwalificatie?.rijbewijs?.forEach(() => {
          this.addRijbewijs()
        })
        this.vraagObject?.arbeidsmarktkwalificatie?.rijbewijs?.forEach(() => {
          this.form.controls['rijbewijs'].setValue(this.vraagObject?.arbeidsmarktkwalificatie?.rijbewijs)
        })
      }
    })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.rijbewijs) {
        this.vraagVacatureObject.rijbewijs?.forEach(() => {
          this.addRijbewijs()
        })
        this.vraagVacatureObject?.rijbewijs?.forEach(() => {
          this.form.controls['rijbewijs'].setValue(this.vraagVacatureObject?.rijbewijs)
        })
      }
    });

    this.form = this.fb.group({
      rijbewijs: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.rijbewijs.valueChanges.subscribe((value) => {
        const entity = {
          rijbewijs: value
        };

        this.valueChangeVacature.emit(entity);
        this.valueChangeWerkzoekende.emit(entity);
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  async addRijbewijs() {
    if (this.t.length < rijbewijsValues.size) {
      this.t.push(this.fb.group({
        codeSoortRijbewijs: ['', [Validators.required]]
      }));
    }
  }

  removeRijbewijs(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.rijbewijs as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
