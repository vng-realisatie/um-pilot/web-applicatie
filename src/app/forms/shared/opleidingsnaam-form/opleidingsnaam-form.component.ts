import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {EMPTY, Observable} from "rxjs";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {MPOpleiding} from "../../../entities/vacature/mPOpleiding";


@Component({
  selector: 'app-opleidingsnaam-form',
  templateUrl: './opleidingsnaam-form.component.html',
  styleUrls: ['./opleidingsnaam-form.component.css']
})
export class OpleidingsnaamFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente


  @Output()
  formReady = new EventEmitter<FormGroup>(true);

  codedValue: number = 0;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      this.vraagVacatureObject.opleiding?.forEach((opleiding) => {


        const opleidingKeys = opleiding.opleidingsnaam ? Object.keys(opleiding.opleidingsnaam) : [];
        const firstKey = opleidingKeys[0];

        if(firstKey == 'mpOpleidingsnaamOngecodeerd') {
          this.codedValue=1
        } if (firstKey == 'opleidingsnaamGecodeerd') {
          this.codedValue=0
        }
      })

    });

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject


      this.vraagObject?.arbeidsmarktkwalificatie?.opleiding?.forEach((opleiding) => {

        const opleidingKeys = opleiding.opleidingsnaam ? Object.keys(opleiding.opleidingsnaam) : [];
        const firstKey = opleidingKeys[0];

        if(firstKey == 'mpOpleidingsnaamOngecodeerd') {
              this.codedValue=1
            } if (firstKey == 'opleidingsnaamGecodeerd') {
              this.codedValue=0
            }

      })
    });

  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  addForm(group: FormGroup) {
    this.formReady.emit(group);
  }

}
