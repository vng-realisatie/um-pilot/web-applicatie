import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {taalBeheersingValues} from "../../../forms-util/formcontrol-value-enums";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";

@Component({
  selector: 'app-taalbeheersing-form',
  templateUrl: './taalbeheersing-form.component.html',
  styleUrls: ['./taalbeheersing-form.component.css']
})
export class TaalbeheersingFormComponent extends AbstractBaseFormComponent implements OnInit {


  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPArbeidsmarktkwalificatie>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  taalBeheersingValues = taalBeheersingValues

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {
    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.arbeidsmarktkwalificatie?.taalbeheersing) {
        this.vraagObject.arbeidsmarktkwalificatie?.taalbeheersing?.forEach(() => {
          this.addTaalbeheersing()
        })
        this.vraagObject?.arbeidsmarktkwalificatie?.taalbeheersing?.forEach(() => {
          this.form.controls['taalbeheersing'].setValue(this.vraagObject?.arbeidsmarktkwalificatie?.taalbeheersing)
        })
      }
    })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.taalbeheersing) {
        this.vraagVacatureObject.taalbeheersing?.forEach(() => {
          this.addTaalbeheersing()
        })
        this.vraagVacatureObject?.taalbeheersing?.forEach(() => {
          this.form.controls['taalbeheersing'].setValue(this.vraagVacatureObject?.taalbeheersing)
        })
      }
    });

    this.form = this.fb.group({
      taalbeheersing: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.taalbeheersing.valueChanges.subscribe((value) => {
        const entity = {
          taalbeheersing: value
        };

        this.valueChangeVacature.emit(entity);
        this.valueChangeWerkzoekende.emit(entity);
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addTaalbeheersing() {
    this.t.push(this.fb.group({
      codeTaal: ['', [Validators.required]],
      codeNiveauTaalbeheersingMondeling: ['', []],
      codeNiveauTaalbeheersingSchriftelijk: ['', []],
      codeNiveauTaalbeheersingLezen: ['', []],
      codeNiveauTaalbeheersingLuisteren: ['', []],
    }));
  }

  removeTaalbeheersing(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.taalbeheersing as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
