import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {BeroepsnaamGecodeerd} from "../../../entities/shared/beroepsnaamGecodeerd";

@Component({
  selector: 'app-beroepsnaam-gecodeerd-form',
  templateUrl: './beroepsnaam-gecodeerd-form.component.html',
  styleUrls: ['./beroepsnaam-gecodeerd-form.component.css']
})
export class BeroepsnaamGecodeerdFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature
  beroep?: BeroepsnaamGecodeerd

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChange = new EventEmitter<Partial<MPVacature>>();

  private subscription = new Subscription();

  form: FormGroup = this.fb.group({})

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject
      this.beroep = <BeroepsnaamGecodeerd>this.vraagObject?.bemiddelingsberoep

      this.form.get('beroepsnaamGecodeerd.codeBeroepsnaam')?.setValue(this.beroep?.beroepsnaamGecodeerd?.codeBeroepsnaam)
      this.form.get('beroepsnaamGecodeerd.omschrijvingBeroepsnaam')?.setValue(this.beroep?.beroepsnaamGecodeerd?.omschrijvingBeroepsnaam)
      })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject
      this.beroep = <BeroepsnaamGecodeerd>this.vraagVacatureObject?.beroep

      this.form.get('beroepsnaamGecodeerd.codeBeroepsnaam')?.setValue(this.beroep?.beroepsnaamGecodeerd?.codeBeroepsnaam)
      this.form.get('beroepsnaamGecodeerd.omschrijvingBeroepsnaam')?.setValue(this.beroep?.beroepsnaamGecodeerd?.omschrijvingBeroepsnaam)
    })


    this.form = this.fb.group({
      beroepsnaamGecodeerd: this.fb.group({
        codeBeroepsnaam: ['', [Validators.maxLength(10), Validators.required]],
        omschrijvingBeroepsnaam: ['', [Validators.maxLength(120)]]
      })
    })

    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {
        this.valueChange.emit({
          beroep: {
            beroepsnaamGecodeerd: {
              codeBeroepsnaam: value.beroepsnaamGecodeerd.codeBeroepsnaam,
              omschrijvingBeroepsnaam: value.beroepsnaamGecodeerd.omschrijvingBeroepsnaam,
            }
          }
        });
      })
    );
    this.formReady.emit(this.form);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
