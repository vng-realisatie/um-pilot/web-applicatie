import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {codeTypeArbeidscontractValues, codeTypeOvereenkomstValues} from "../../../forms-util/formcontrol-value-enums";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {convertToDateStruct} from "../../../util/convertToDateStruct";

@Component({
  selector: 'app-contractvorm-form',
  templateUrl: './contractvorm-form.component.html',
  styleUrls: ['./contractvorm-form.component.css']
})
export class ContractvormFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  codeTypeArbeidscontractValues = codeTypeArbeidscontractValues;
  codeTypeOvereenkomstValues = codeTypeOvereenkomstValues;


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.contractvorm) {
        this.vraagObject.contractvorm?.forEach(() => {
          this.addContractvorm()
        })
        this.vraagObject?.contractvorm?.forEach(() => {
          this.form.controls['contractvorm'].setValue(this.vraagObject?.contractvorm)
        })
      }
    })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.contractvorm) {
        this.vraagVacatureObject.contractvorm?.forEach(() => {
          this.addContractvorm()
        })
        this.vraagVacatureObject?.contractvorm?.forEach(() => {
          this.form.controls['contractvorm'].setValue(this.vraagVacatureObject?.contractvorm)
        })
      }
    });

    this.form = this.fb.group({
      contractvorm: this.fb.array([]),
    });

    this.subscription.add(
      this.form.controls.contractvorm.valueChanges.subscribe((value) => {
        const entity = {
          contractvorm: value
        };
        this.valueChangeVacature.emit(entity);
        this.valueChangeWerkzoekende.emit(entity);
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addContractvorm() {
    this.t.push(this.fb.group({
      codeTypeArbeidscontract: ['', [Validators.required]],
      codeTypeOvereenkomst: ['', [Validators.required]]
    }));
  }

  removeContractvorm(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.contractvorm as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
