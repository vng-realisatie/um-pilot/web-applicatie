import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {indicatieThreeValues} from "../../../forms-util/formcontrol-value-enums";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";

@Component({
  selector: 'app-vervoermiddel-form',
  templateUrl: './vervoermiddel-form.component.html',
  styleUrls: ['./vervoermiddel-form.component.css']
})
export class VervoermiddelFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  indicatieThreeValues = indicatieThreeValues;


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.vervoermiddel) {
        this.vraagObject.vervoermiddel?.forEach(() => {
          this.addVervoermiddel()
        })
        this.vraagObject?.vervoermiddel?.forEach(() => {
          this.form.controls['vervoermiddel'].setValue(this.vraagObject?.vervoermiddel)
        })
      }
    })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.vervoermiddel) {
        this.vraagVacatureObject.vervoermiddel?.forEach(() => {
          this.addVervoermiddel()
        })
        this.vraagVacatureObject?.sollicitatiewijze?.forEach(() => {
          this.form.controls['vervoermiddel'].setValue(this.vraagVacatureObject?.vervoermiddel)
        })
      }
    });

    this.form = this.fb.group({
      vervoermiddel: this.fb.array([]),
    });

    this.subscription.add(
      this.form.controls.vervoermiddel.valueChanges.subscribe((value) => {
        this.valueChangeVacature.emit({
          vervoermiddel: value
        });

        this.valueChangeWerkzoekende.emit({
          vervoermiddel: value
        })

      })
    );


    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }


  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addVervoermiddel() {
    this.t.push(this.fb.group({
      indicatieBeschikbaarVoorUitvoeringWerk: ['', [Validators.required]],
      indicatieBeschikbaarVoorWoonWerkverkeer: ['', [Validators.required]]
    }));
  }

  removeVervoermiddel(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.vervoermiddel as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
