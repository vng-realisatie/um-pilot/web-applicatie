import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {indicatieThreeValues} from "../../../forms-util/formcontrol-value-enums";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";


@Component({
  selector: 'app-werktijden-form',
  templateUrl: './werktijden-form.component.html',
  styleUrls: ['./werktijden-form.component.css']
})
export class WerktijdenFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  indicatieThreeValues = indicatieThreeValues;


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {
    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      this.form.controls['aantalWerkurenPerWeekMinimaal'].setValue(this.vraagVacatureObject?.werktijden?.aantalWerkurenPerWeekMinimaal)
      this.form.controls['aantalWerkurenPerWeekMaximaal'].setValue(this.vraagVacatureObject?.werktijden?.aantalWerkurenPerWeekMaximaal)
      this.form.controls['indicatieKantoortijden'].setValue(this.vraagVacatureObject?.werktijden?.indicatieKantoortijden)
    });

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject
      this.form.controls['aantalWerkurenPerWeekMinimaal'].setValue(this.vraagObject?.werktijden?.aantalWerkurenPerWeekMinimaal)
      this.form.controls['aantalWerkurenPerWeekMaximaal'].setValue(this.vraagObject?.werktijden?.aantalWerkurenPerWeekMaximaal)
      this.form.controls['indicatieKantoortijden'].setValue(this.vraagObject?.werktijden?.indicatieKantoortijden)

    })

    this.form = this.fb.group({
      aantalWerkurenPerWeekMinimaal: ['', [Validators.min(0), Validators.max(99)]],
      aantalWerkurenPerWeekMaximaal: ['', [Validators.min(0), Validators.max(99)]],
      indicatieKantoortijden: ['', []],

    });

    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {
        const entity = {
          werktijden: {
            aantalWerkurenPerWeekMinimaal: value.aantalWerkurenPerWeekMinimaal,
            aantalWerkurenPerWeekMaximaal: value.aantalWerkurenPerWeekMaximaal,
            indicatieKantoortijden: value.indicatieKantoortijden
          }
        }
        this.valueChangeVacature.emit(entity);
        this.valueChangeWerkzoekende.emit(entity);
      })
    );

    this.formReady.emit(this.form);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
