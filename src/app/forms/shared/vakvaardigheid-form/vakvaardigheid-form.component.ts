import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";

@Component({
  selector: 'app-vakvaardigheid-form',
  templateUrl: './vakvaardigheid-form.component.html',
  styleUrls: ['./vakvaardigheid-form.component.css']
})
export class VakvaardigheidFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPArbeidsmarktkwalificatie>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.arbeidsmarktkwalificatie?.vakvaardigheid) {
        this.vraagObject.arbeidsmarktkwalificatie?.vakvaardigheid?.forEach(() => {
          this.addVakvaardigheid()
        })
        this.vraagObject?.arbeidsmarktkwalificatie?.vakvaardigheid?.forEach(() => {
          this.form.controls['vakvaardigheid'].setValue(this.vraagObject?.arbeidsmarktkwalificatie?.vakvaardigheid)
        })
      }
    })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.vakvaardigheid) {
        this.vraagVacatureObject.vakvaardigheid?.forEach(() => {
          this.addVakvaardigheid()
        })
        this.vraagVacatureObject?.vakvaardigheid?.forEach(() => {
          this.form.controls['vakvaardigheid'].setValue(this.vraagVacatureObject?.vakvaardigheid)
        })
      }
    });

    this.form = this.fb.group({
      vakvaardigheid: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.vakvaardigheid.valueChanges.subscribe((value) => {
        const entity = {
          vakvaardigheid: value
        };
        this.valueChangeVacature.emit(entity);
        this.valueChangeWerkzoekende.emit(entity);
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }


  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addVakvaardigheid() {
    this.t.push(this.fb.group({
      omschrijving: ['', [Validators.required, Validators.maxLength(120)]]
    }));

  }

  removeVakvaardigheid(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.vakvaardigheid as FormArray;
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
