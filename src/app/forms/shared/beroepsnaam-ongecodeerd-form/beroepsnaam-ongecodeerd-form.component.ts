import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {BeroepsnaamGecodeerd} from "../../../entities/shared/beroepsnaamGecodeerd";
import {BeroepsnaamOngecodeerd} from "../../../entities/shared/beroepsnaamOngecodeerd";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";

@Component({
  selector: 'app-beroepsnaam-ongecodeerd-form',
  templateUrl: './beroepsnaam-ongecodeerd-form.component.html',
  styleUrls: ['./beroepsnaam-ongecodeerd-form.component.css']
})
export class BeroepsnaamOngecodeerdFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature
  beroep?: BeroepsnaamOngecodeerd

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChange = new EventEmitter<Partial<MPVacature>>();

  private subscription = new Subscription();

  form: FormGroup = this.fb.group({})

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject
      this.beroep = <BeroepsnaamOngecodeerd>this.vraagObject.bemiddelingsberoep

      this.form.get('beroepsnaamOngecodeerd.naamBeroepOngecodeerd')?.setValue(this.beroep.beroepsnaamOngecodeerd?.naamBeroepOngecodeerd)
    })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject
      this.beroep = <BeroepsnaamOngecodeerd>this.vraagVacatureObject?.beroep

      this.form.get('beroepsnaamOngecodeerd.naamBeroepOngecodeerd')?.setValue(this.beroep.beroepsnaamOngecodeerd?.naamBeroepOngecodeerd)
    });

    this.form = this.fb.group({
      beroepsnaamOngecodeerd: this.fb.group({
        naamBeroepOngecodeerd: ['', [Validators.maxLength(120)]]
      })
    })

    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {
        this.valueChange.emit({
          beroep: {
            beroepsnaamOngecodeerd: {
              naamBeroepOngecodeerd: value.beroepsnaamOngecodeerd.naamBeroepOngecodeerd
            }
          }
        });
      })
    );
    this.formReady.emit(this.form);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
