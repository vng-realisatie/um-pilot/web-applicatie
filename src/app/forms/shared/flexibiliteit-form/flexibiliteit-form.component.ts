import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {codeRegioStraalValues, indicatieThreeValues} from "../../../forms-util/formcontrol-value-enums";
import {EMPTY, observable, Observable, Subscription} from "rxjs";
import {dateFormatterNgbDate, dateFormatterString} from "../../../util/dateformatter";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {convertToDateStruct} from "../../../util/convertToDateStruct";


@Component({
  selector: 'app-flexibiliteit-form',
  templateUrl: './flexibiliteit-form.component.html',
  styleUrls: ['./flexibiliteit-form.component.css']
})
export class FlexibiliteitFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  codeRegioStraalValues = codeRegioStraalValues;
  indicatieThreeValues = indicatieThreeValues;


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      const startDate = convertToDateStruct(this.vraagVacatureObject?.flexibiliteit?.datumAanvangBeschikbaarVoorWerk);
      this.form.controls['datumAanvangBeschikbaarVoorWerk'].setValue(startDate);

      const endDate = convertToDateStruct(this.vraagVacatureObject?.flexibiliteit?.datumEindeBeschikbaarVoorWerk);
      this.form.controls['datumEindeBeschikbaarVoorWerk'].setValue(endDate);

      this.form.controls['codeRegiostraal'].setValue(this.vraagVacatureObject?.flexibiliteit?.codeRegiostraal)
      this.form.controls['indicatieOnregelmatigWerkOfPloegendienst'].setValue(this.vraagVacatureObject?.flexibiliteit?.indicatieOnregelmatigWerkOfPloegendienst)
    });

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      const startDate = convertToDateStruct(this.vraagObject?.flexibiliteit?.datumAanvangBeschikbaarVoorWerk);
      this.form.controls['datumAanvangBeschikbaarVoorWerk'].setValue(startDate);

      const endDate = convertToDateStruct(this.vraagObject?.flexibiliteit?.datumEindeBeschikbaarVoorWerk);
      this.form.controls['datumEindeBeschikbaarVoorWerk'].setValue(endDate);

      this.form.controls['codeRegiostraal'].setValue(this.vraagObject?.flexibiliteit?.codeRegiostraal)
      this.form.controls['indicatieOnregelmatigWerkOfPloegendienst'].setValue(this.vraagObject?.flexibiliteit?.indicatieOnregelmatigWerkOfPloegendienst)

    })

    this.form = this.fb.group({
      codeRegiostraal: ['', []],
      indicatieOnregelmatigWerkOfPloegendienst: ['', []],
      datumAanvangBeschikbaarVoorWerk: [null, []],
      datumEindeBeschikbaarVoorWerk: [null, []],
    });

    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {

        const entity = {
          flexibiliteit: {
            codeRegiostraal: value.codeRegiostraal,
            indicatieOnregelmatigWerkOfPloegendienst: value.indicatieOnregelmatigWerkOfPloegendienst,
            datumAanvangBeschikbaarVoorWerk: dateFormatterNgbDate(value.datumAanvangBeschikbaarVoorWerk),
            datumEindeBeschikbaarVoorWerk: dateFormatterNgbDate(value.datumEindeBeschikbaarVoorWerk)
          }
        }

        this.valueChangeVacature.emit(entity);
        this.valueChangeWerkzoekende.emit(entity);
      })
    );

    this.formReady.emit(this.form);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
