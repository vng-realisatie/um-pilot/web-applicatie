import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {MPWerkervaring} from "../../../entities/werkzoekende/mPWerkervaring";
import {EMPTY, Observable} from "rxjs";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {values} from "ag-grid-community/dist/lib/utils/generic";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";

@Component({
  selector: 'app-beroep-form',
  templateUrl: './beroep-form.component.html',
  styleUrls: ['./beroep-form.component.css']
})
export class BeroepFormComponent implements OnInit {

  @Input() value: any;

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormGroup>(true);

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkervaring>>();

  codedValue: number = 0;
  testarray: any = [];

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
  }

  ngOnInit(): void {
    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      const beroepKeys = this.vraagVacatureObject.beroep ? Object.keys(this.vraagVacatureObject.beroep) : [];
      const firstKey = beroepKeys[0];

      if(firstKey == 'beroepsnaamOngecodeerd') {
        this.codedValue=2
      } if (firstKey == 'beroepsnaamGecodeerd') {
        this.codedValue=1
      }
    });

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      this.vraagObject?.bemiddelingsberoep?.forEach((beroepsnaam) => {

      })
    });




  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  addForm(group: FormGroup) {
    this.formReady.emit(group);
  }

  onValueChangeVacature(changes: Partial<MPVacature>) {
    this.valueChangeVacature.emit(changes);
  }

  onValueChangeWerkzoekende(changes: Partial<MPWerkervaring>) {
    this.valueChangeWerkzoekende.emit(changes);
  }


  onSelectedChange(value: number) {
    if (value === 0) {
      this.formReady.emit(new FormGroup({}));
      const entity = {
        beroep: undefined
      };
      this.valueChangeVacature.emit(entity);
      this.valueChangeWerkzoekende.emit(entity);
    }
  }
}
