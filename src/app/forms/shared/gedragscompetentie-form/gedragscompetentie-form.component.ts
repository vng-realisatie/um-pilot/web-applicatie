import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
  beheersingGedragscompetentieValues,
  codeGedragscompetentieValues
} from "../../../forms-util/formcontrol-value-enums";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";

@Component({
  selector: 'app-gedragscompetentie-form',
  templateUrl: './gedragscompetentie-form.component.html',
  styleUrls: ['./gedragscompetentie-form.component.css']
})
export class GedragscompetentieFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPArbeidsmarktkwalificatie>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  codeGedragscompetentieValues = codeGedragscompetentieValues;
  beheersingGedragscompetentieValues = beheersingGedragscompetentieValues;


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.arbeidsmarktkwalificatie?.gedragscompetentie) {
        this.vraagObject.arbeidsmarktkwalificatie?.gedragscompetentie?.forEach(() => {
          this.addGedragscompetentie()
        })
        this.vraagObject?.arbeidsmarktkwalificatie?.gedragscompetentie?.forEach(() => {
          this.form.controls['gedragscompetentie'].setValue(this.vraagObject?.arbeidsmarktkwalificatie?.gedragscompetentie)
        })
      }
    })

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.gedragscompetentie) {
        this.vraagVacatureObject.gedragscompetentie?.forEach(() => {
          this.addGedragscompetentie()
        })
        this.vraagVacatureObject?.gedragscompetentie?.forEach(() => {
          this.form.controls['gedragscompetentie'].setValue(this.vraagVacatureObject?.gedragscompetentie)
        })
      }
    });

    this.form = this.fb.group({
      gedragscompetentie: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.gedragscompetentie.valueChanges.subscribe((value) => {
        const entity = {
          gedragscompetentie: value
        };


        this.valueChangeVacature.emit(entity);
        this.valueChangeWerkzoekende.emit(entity)
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addGedragscompetentie() {
    this.t.push(this.fb.group({
      codeGedragscompetentie: ['', []],
      omschrijvingGedragscompetentie: ['', [Validators.maxLength(120)]],
      codeBeheersingGedragscompetentie: ['', []]
    }));
  }

  removeGedragscompetentie(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.gedragscompetentie as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
