import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {codeWerkEnDenkniveauMinimaalValues} from "../../../forms-util/formcontrol-value-enums";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";

@Component({
  selector: 'app-arbeidsmarktkwalificatie-form',
  templateUrl: './arbeidsmarktkwalificatie-form.component.html',
  styleUrls: ['./arbeidsmarktkwalificatie-form.component.css']
})
export class ArbeidsmarktkwalificatieFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>(true);

  form: FormGroup = this.fb.group({});

  arbeidsmarktkwalificatie: MPArbeidsmarktkwalificatie = {};

  codeWerkEnDenkniveauValues = codeWerkEnDenkniveauMinimaalValues


  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {
    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      this.form.controls['codeWerkEnDenkniveauWerkzoekende'].setValue(this.vraagObject?.arbeidsmarktkwalificatie?.codeWerkEnDenkniveauWerkzoekende)
    })

    this.form = this.fb.group({
      codeWerkEnDenkniveauWerkzoekende: ['', []]
    })

    this.subscription.add(
      this.form.controls.codeWerkEnDenkniveauWerkzoekende.valueChanges.subscribe((value) => {
        //trigger emit even when only codewerkendenkniveau is filled
        this.onValueChange({})
      })
    );

    this.formReady.emit(this.form);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }


  addChildFormArray(name: string, array: FormArray) {
    this.form.addControl(name, array);
  }

  onValueChange(changes: Partial<MPArbeidsmarktkwalificatie>) {
    this.arbeidsmarktkwalificatie = {
      ...this.arbeidsmarktkwalificatie,
      ...changes,
      codeWerkEnDenkniveauWerkzoekende: this.form.controls.codeWerkEnDenkniveauWerkzoekende.value
    };
    this.valueChangeWerkzoekende.emit({arbeidsmarktkwalificatie: this.arbeidsmarktkwalificatie})
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
