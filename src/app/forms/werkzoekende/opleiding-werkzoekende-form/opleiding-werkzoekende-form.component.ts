import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {codeNiveauOpleidingValues, indicatieDiplomaValues} from "../../../forms-util/formcontrol-value-enums";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {dateFormatterNgbDate} from "../../../util/dateformatter";
import {MPOpleiding} from "../../../entities/werkzoekende/mPOpleiding";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {convertToDateStruct} from "../../../util/convertToDateStruct";


@Component({
  selector: 'app-opleiding-werkzoekende-form',
  templateUrl: './opleiding-werkzoekende-form.component.html',
  styleUrls: ['./opleiding-werkzoekende-form.component.css']
})
export class OpleidingWerkzoekendeFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  opleidingArray?: any = [];

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPArbeidsmarktkwalificatie>>();

  codeNiveauOpleidingValues = codeNiveauOpleidingValues;
  indicatieDiplomaValues = indicatieDiplomaValues;

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.arbeidsmarktkwalificatie?.opleiding) {
        this.vraagObject.arbeidsmarktkwalificatie?.opleiding?.forEach((opleiding) => {
          this.addOpleiding()

          const datum = opleiding?.datumDiploma
          const datumCertificaat = convertToDateStruct(datum);
          const codeNiveauOpleiding = opleiding?.codeNiveauOpleiding
          const indicatieDiploma = opleiding?.indicatieDiploma

          const opleidingKeys = opleiding.opleidingsnaam ? Object.keys(opleiding.opleidingsnaam) : [];
          const firstKey = opleidingKeys[0];

          var newCursusObject = {
            codeNiveauOpleiding: codeNiveauOpleiding,
            indicatieDiploma: indicatieDiploma,
            datumDiploma: datumCertificaat,
            opleidingsnaam: firstKey
          }

          this.opleidingArray.push(newCursusObject);


        })
        setTimeout(() => {
          this.vraagObject?.arbeidsmarktkwalificatie?.opleiding?.forEach(() => {
            //this.form.controls['opleiding'].setValue(this.opleidingArray)
          })
        })
      }
    })

    this.form = this.fb.group({
      opleiding: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.opleiding.valueChanges.subscribe((value) => {

        const mappedValues = value.map((x: MPOpleiding) => {
          return {
            codeNiveauOpleiding: x.codeNiveauOpleiding,
            indicatieDiploma: x.indicatieDiploma,
            datumDiploma: dateFormatterNgbDate(x.datumDiploma),
            opleidingsnaam: x.opleidingsnaam
          }
        });

        this.valueChangeWerkzoekende.emit({
          opleiding: mappedValues
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }


  addOpleiding() {
    this.t.push(this.fb.group({
      codeNiveauOpleiding: ['', []],
      indicatieDiploma: ['', []],
      datumDiploma: [null, []],
      opleidingsnaam: this.fb.group({})
    }));
  }

  removeOpleiding(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.opleiding as FormArray;
  }

  updateOpleidingsnaam(opleidingControl: FormGroup, group: FormGroup) {
    opleidingControl.setControl('opleidingsnaam', group);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
