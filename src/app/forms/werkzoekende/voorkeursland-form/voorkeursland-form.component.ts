import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import * as i18nIsoCountries from 'i18n-iso-countries';
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";

@Component({
  selector: 'app-voorkeursland-form',
  templateUrl: './voorkeursland-form.component.html',
  styleUrls: ['./voorkeursland-form.component.css']
})
export class VoorkeurslandFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  countryValues = new Map();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {
    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.voorkeursland) {
        this.vraagObject.voorkeursland?.forEach(() => {
          this.addVoorkeursland()
        })
        this.vraagObject?.voorkeursland?.forEach(() => {
          this.form.controls['voorkeursland'].setValue(this.vraagObject?.voorkeursland)
        })
      }
    })

    i18nIsoCountries.registerLocale(require("i18n-iso-countries/langs/nl.json"));

    this.countryValues = new Map(Object.entries(i18nIsoCountries.getNames("nl", {select: "official"})))

    this.form = this.fb.group({
      voorkeursland: this.fb.array([]),
    });

    this.subscription.add(
      this.form.controls.voorkeursland.valueChanges.subscribe((value) => {
        this.valueChangeWerkzoekende.emit({
          voorkeursland: value
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addVoorkeursland() {
    this.t.push(this.fb.group({
      landencodeIso: ['', [Validators.required]]
    }));
  }

  removeVoorkeursland(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.voorkeursland as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
