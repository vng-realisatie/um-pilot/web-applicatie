import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";

@Component({
  selector: 'app-mobiliteit-form',
  templateUrl: './mobiliteit-form.component.html',
  styleUrls: ['./mobiliteit-form.component.css']
})
export class MobiliteitFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {
    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.form = this.fb.group({
      bemiddelingspostcode: ['', [Validators.required, Validators.pattern(/^[1-9][0-9]{3}(?!sa|sd|ss)[a-z]{2}$/i)]],
      maximaleReisafstand: ['', [Validators.required, Validators.min(0), Validators.max(999)]],
      maximaleReistijd: ['', [Validators.min(0), Validators.max(999)]]
    });


    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject
      this.form.controls['bemiddelingspostcode'].setValue(this.vraagObject?.mobiliteit?.bemiddelingspostcode)
      this.form.controls['maximaleReisafstand'].setValue(this.vraagObject?.mobiliteit?.maximaleReisafstand)
      this.form.controls['maximaleReistijd'].setValue(this.vraagObject?.mobiliteit?.maximaleReistijd)

    })

    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {
        this.valueChangeWerkzoekende.emit({
          mobiliteit: {
            bemiddelingspostcode: value.bemiddelingspostcode,
            maximaleReisafstand: value.maximaleReisafstand,
            maximaleReistijd: value.maximaleReistijd
          }
        });
      })
    );

    this.formReady.emit(this.form);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
