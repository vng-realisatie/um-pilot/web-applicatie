import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {Beroepsnaam} from "../../../entities/shared/beroepsnaam";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";

@Component({
  selector: 'app-bemiddelingsberoep-werkzoekende-form',
  templateUrl: './bemiddelingsberoep-werkzoekende-form.component.html',
  styleUrls: ['./bemiddelingsberoep-werkzoekende-form.component.css']
})
export class BemiddelingsberoepWerkzoekendeFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;
  beroep?: Beroepsnaam

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      this.beroep = <Beroepsnaam>this.vraagObject.bemiddelingsberoep

      if(this.vraagObject?.bemiddelingsberoep) {
        this.vraagObject.bemiddelingsberoep?.forEach(() => {
          this.addBeroep()
        })
      }
    })

    this.form = this.fb.group({
      bemiddelingsberoep: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.bemiddelingsberoep.valueChanges.subscribe((value) => {
        const mappedValue = value.map((x: { beroepsnaam: Beroepsnaam }) => {
            return x.beroepsnaam
          }
        );
        this.valueChangeWerkzoekende.emit({
          bemiddelingsberoep: mappedValue
        });
      })
    );
    this.formReady.emit(this.t);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addBeroep() {
    this.t.push(this.fb.group({
      beroepsnaam: this.fb.group({})
    }));
  }

  removeBeroep(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.bemiddelingsberoep as FormArray;
  }

  updateBeroep(beroepControl: FormGroup, group: FormGroup) {
    beroepControl.setControl('beroepsnaam', group);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
