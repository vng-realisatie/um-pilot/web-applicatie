import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {EMPTY, Observable, Subscription} from "rxjs";
import {dateFormatterNgbDate} from "../../../util/dateformatter";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {MPWerkervaring} from "../../../entities/werkzoekende/mPWerkervaring";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";

@Component({
  selector: 'app-werkervaring-werkzoekende-form',
  templateUrl: './werkervaring-werkzoekende-form.component.html',
  styleUrls: ['./werkervaring-werkzoekende-form.component.css']
})
export class WerkervaringWerkzoekendeFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPArbeidsmarktkwalificatie>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.arbeidsmarktkwalificatie?.werkervaring) {
        this.vraagObject.arbeidsmarktkwalificatie?.werkervaring?.forEach(() => {
          this.addWerkervaring()
        })
        this.vraagObject?.arbeidsmarktkwalificatie?.werkervaring?.forEach(() => {
          //this.form.controls['werkervaring'].setValue(this.vraagObject?.arbeidsmarktkwalificatie?.werkervaring)
        })
      }
    })


    this.form = this.fb.group({
      werkervaring: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.werkervaring.valueChanges.subscribe((value) => {

        const mappedValues = value.map((x: MPWerkervaring) => {
          return {
            datumAanvangWerkzaamheden: dateFormatterNgbDate(x.datumAanvangWerkzaamheden),
            datumEindeWerkzaamheden: dateFormatterNgbDate(x.datumEindeWerkzaamheden),
            naamOrganisatie: x.naamOrganisatie,
            beroep: x.beroep
          }
        });

        this.valueChangeWerkzoekende.emit({
          werkervaring: mappedValues
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }


  addWerkervaring() {
    this.t.push(this.fb.group({
      datumAanvangWerkzaamheden: [null, []],
      datumEindeWerkzaamheden: [null, []],
      naamOrganisatie: ['', [Validators.minLength(0), Validators.maxLength(70)]],
      beroep: this.fb.group({})
    }));
  }

  removeWerkervaring(i: number) {
    this.t.removeAt(i);
  }

  updateWerkervaring(werkervaringControl: FormGroup, group: FormGroup) {
    werkervaringControl.setControl('beroep', group);
  }

  get t() {
    return this.f.werkervaring as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
