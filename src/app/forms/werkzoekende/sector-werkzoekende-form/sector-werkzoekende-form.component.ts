import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";

@Component({
  selector: 'app-sector-werkzoekende-form',
  templateUrl: './sector-werkzoekende-form.component.html',
  styleUrls: ['./sector-werkzoekende-form.component.css']
})
export class SectorWerkzoekendeFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      if(this.vraagObject?.sector) {
        this.vraagObject.sector?.forEach(() => {
          this.addSector()
        })
        this.vraagObject?.sector?.forEach(() => {
          this.form.controls['sector'].setValue(this.vraagObject?.sector)
        })
      }
    })

    this.form = this.fb.group({
      sector: this.fb.array([]),
    });

    this.subscription.add(
      this.form.controls.sector.valueChanges.subscribe((value) => {
        this.valueChangeWerkzoekende.emit({
          sector: value
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addSector() {
    this.t.push(this.fb.group({
      codeSbi: ['', [Validators.min(0), Validators.max(99999)]]
    }));
  }

  removeSector(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.sector as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
