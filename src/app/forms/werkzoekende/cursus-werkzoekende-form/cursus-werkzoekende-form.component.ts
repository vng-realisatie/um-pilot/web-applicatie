import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {MPCursus} from "../../../entities/werkzoekende/mPCursus";
import {dateFormatterNgbDate} from "../../../util/dateformatter";
import {MPArbeidsmarktkwalificatie} from "../../../entities/werkzoekende/mPArbeidsmarktkwalificatie";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {convertToDateStruct} from "../../../util/convertToDateStruct";

@Component({
  selector: 'app-cursus-werkzoekende-form',
  templateUrl: './cursus-werkzoekende-form.component.html',
  styleUrls: ['./cursus-werkzoekende-form.component.css']
})
export class CursusWerkzoekendeFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  cursusArray?: any = [];
  cursus?: MPCursus
  arbeidsKwalificaties?: MPArbeidsmarktkwalificatie

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPArbeidsmarktkwalificatie>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject

      this.arbeidsKwalificaties = <MPArbeidsmarktkwalificatie>this.vraagObject?.arbeidsmarktkwalificatie

        if (this.arbeidsKwalificaties?.cursus) {
          this.arbeidsKwalificaties?.cursus.forEach((cursus) => {
            this.addCursus()
            const datum = cursus?.datumCertificaat
            const datumCertificaat = convertToDateStruct(datum);
            const naam = cursus?.naamCursus

            var newCursusObject = {
              naamCursus: naam,
              datumCertificaat: datumCertificaat
            }
            this.cursusArray.push(newCursusObject);
          })

          this.arbeidsKwalificaties?.cursus?.forEach(() => {
            this.form.controls['cursus'].setValue(this.cursusArray);
        })
      }
    })

    this.form = this.fb.group({
      cursus: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.cursus.valueChanges.subscribe((value) => {

        const mappedValues = value.map((x: MPCursus) => {
          return {naamCursus: x.naamCursus, datumCertificaat: dateFormatterNgbDate(x.datumCertificaat)}
        });
        this.valueChangeWerkzoekende.emit({
          cursus: mappedValues,
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  addCursus() {
    this.t.push(this.fb.group({
      naamCursus: ['', [Validators.required, Validators.maxLength(120)]],
      datumCertificaat: [null, []]
    }));
  }

  removeCursus(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.cursus as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
