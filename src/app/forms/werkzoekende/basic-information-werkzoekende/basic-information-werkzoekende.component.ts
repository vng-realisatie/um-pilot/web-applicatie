import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {indicatieTwoValues} from "../../../forms-util/formcontrol-value-enums";
import {EMPTY, Observable, Subscription} from "rxjs";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {MPWerkzoekende} from "../../../entities/werkzoekende/mPWerkzoekende";
import {AanvraagWerkzoekende} from "../../../entities/werkzoekende/aanvraagWerkzoekende";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {WerkzoekendeBemiddelaarService} from "../../../services/werkzoekende-bemiddelaar.service";

@Component({
  selector: 'app-basic-information-werkzoekende',
  templateUrl: './basic-information-werkzoekende.component.html',
  styleUrls: ['./basic-information-werkzoekende.component.css']
})
export class BasicInformationWerkzoekendeComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagWerkzoekende$: Observable<AanvraagWerkzoekende> = EMPTY;
  vraagId?: string | null;

  zoekparameters?: MatchesRequestGemeente
  vraagObject?: MPWerkzoekende

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChangeWerkzoekende = new EventEmitter<Partial<MPWerkzoekende>>();

  @Output()
  valueChangesPostcodeAndStraal = new EventEmitter<Partial<{ postcode: string, straal: number, aanvraagKenmerk: string }>>();

  indicatieValues = indicatieTwoValues;

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private werkzoekendeService: WerkzoekendeBemiddelaarService) {
    super();
  }

  ngOnInit(): void {
    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getWerkzoekende();

    this.form = this.fb.group({
      aanvraagKenmerk: ['', [Validators.required]],
      indicatieLdrRegistratie: ['', []],
      indicatieBeschikbaarheidContactgegevens: ['', []],
    });

    this.aanvraagWerkzoekende$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagObject = <MPWerkzoekende>this.zoekparameters?.vraagObject
      this.form.controls['aanvraagKenmerk'].setValue(this.zoekparameters?.aanvraagKenmerk)
      this.form.controls['indicatieLdrRegistratie'].setValue(this.vraagObject?.indicatieLdrRegistratie)
      this.form.controls['indicatieBeschikbaarheidContactgegevens'].setValue(this.vraagObject?.indicatieBeschikbaarheidContactgegevens)

    })


    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {
        this.valueChangeWerkzoekende.emit({
          indicatieLdrRegistratie: value.indicatieLdrRegistratie,
          indicatieBeschikbaarheidContactgegevens: value.indicatieBeschikbaarheidContactgegevens
        })

        this.valueChangesPostcodeAndStraal.emit({
          aanvraagKenmerk: value.aanvraagKenmerk
        })
      })
    );

    this.formReady.emit(this.form);
  }

  private getWerkzoekende(): void {
    if (this.vraagId) {
      this.aanvraagWerkzoekende$ = this.werkzoekendeService.getVraagId(this.vraagId);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
