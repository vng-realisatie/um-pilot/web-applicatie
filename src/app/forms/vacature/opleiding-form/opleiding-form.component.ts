import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {codeNiveauOpleidingValues, indicatieDiplomaValues} from "../../../forms-util/formcontrol-value-enums";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {EMPTY, Observable, Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";

@Component({
  selector: 'app-opleiding-form',
  templateUrl: './opleiding-form.component.html',
  styleUrls: ['./opleiding-form.component.css']
})
export class OpleidingFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente


  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  codeNiveauOpleidingValues = codeNiveauOpleidingValues;
  indicatieDiplomaValues = indicatieDiplomaValues;

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.opleiding) {
        this.vraagVacatureObject.opleiding?.forEach(() => {
          this.addOpleiding()
        })
        this.vraagVacatureObject?.opleiding?.forEach(() => {
          //this.form.controls['opleiding'].setValue(this.vraagVacatureObject?.opleiding)
        })
      }
    });

    this.form = this.fb.group({
      opleiding: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.opleiding.valueChanges.subscribe((value) => {
        this.valueChangeVacature.emit({
          opleiding: value
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  addOpleiding() {
    this.t.push(this.fb.group({
      codeNiveauOpleiding: ['', [Validators.required]],
      indicatieDiploma: ['', [Validators.required]],
      opleidingsnaam: this.fb.group({})
    }));
  }

  removeOpleiding(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.opleiding as FormArray;
  }

  updateOpleidingsnaam(opleidingControl: FormGroup, group: FormGroup) {
    opleidingControl.setControl('opleidingsnaam', group);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
