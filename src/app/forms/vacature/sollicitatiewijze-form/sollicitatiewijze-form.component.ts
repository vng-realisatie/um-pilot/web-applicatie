import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {sollicitatieWijzeValues} from "../../../forms-util/formcontrol-value-enums";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";

@Component({
  selector: 'app-sollicitatiewijze-form',
  templateUrl: './sollicitatiewijze-form.component.html',
  styleUrls: ['./sollicitatiewijze-form.component.css']
})
export class SollicitatiewijzeFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  sollicitatieWijzeValues = sollicitatieWijzeValues


  constructor(private fb: FormBuilder, private route: ActivatedRoute, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.sollicitatiewijze) {
        this.vraagVacatureObject.sollicitatiewijze?.forEach(() => {
          this.addSollicitatiewijze()
        })
        this.vraagVacatureObject?.sollicitatiewijze?.forEach(() => {
          this.form.controls['sollicitatiewijze'].setValue(this.vraagVacatureObject?.sollicitatiewijze)
        })
      }
    });

    this.form = this.fb.group({
      sollicitatiewijze: this.fb.array([]),
    });

    this.subscription.add(
      this.form.controls.sollicitatiewijze.valueChanges.subscribe((value) => {
        this.valueChangeVacature.emit({
          sollicitatiewijze: value
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }


  addSollicitatiewijze() {
    if (this.t.length < sollicitatieWijzeValues.size) {
      this.t.push(this.fb.group({
        codeSollicitatiewijze: ['', [Validators.required]]
      }));
    }
  }

  removeSollicitatieWijze(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.sollicitatiewijze as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
