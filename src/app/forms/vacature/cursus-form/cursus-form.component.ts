import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";

@Component({
  selector: 'app-cursus-form',
  templateUrl: './cursus-form.component.html',
  styleUrls: ['./cursus-form.component.css']
})
export class CursusFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.cursus) {
        this.vraagVacatureObject.cursus?.forEach(() => {
          this.addCursus()
        })
        this.vraagVacatureObject?.cursus?.forEach(() => {
          this.form.controls['cursus'].setValue(this.vraagVacatureObject?.cursus)
        })
      }
    });

    this.form = this.fb.group({
      cursus: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.cursus.valueChanges.subscribe((value) => {
        this.valueChangeVacature.emit({
          cursus: value
        });
      })
    );

    this.formReady.emit(this.t);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }


  addCursus() {
    this.t.push(this.fb.group({
      naamCursus: ['', [Validators.required, Validators.maxLength(120)]]
    }));
  }

  removeCursus(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.cursus as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
