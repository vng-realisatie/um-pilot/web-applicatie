import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {codeWerkEnDenkniveauMinimaalValues, indicatieTwoValues} from "../../../forms-util/formcontrol-value-enums"
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {EMPTY, Observable, Subscription} from "rxjs";
import {dateFormatterNgbDate} from "../../../util/dateformatter";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {convertToDateStruct} from "../../../util/convertToDateStruct";

@Component({
  selector: 'app-basic-information-vacature',
  templateUrl: './basic-information-vacature.component.html',
  styleUrls: ['./basic-information-vacature.component.css']
})
export class BasicInformationVacatureComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  @Output()
  valueChangesAanvraagkenmerk = new EventEmitter<Partial<{ aanvraagKenmerk: string }>>();

  codeWerkEnDenkniveauMinimaalValues = codeWerkEnDenkniveauMinimaalValues;
  indicatieValues = indicatieTwoValues;

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      const sluitingsDatumVacature = convertToDateStruct(this.vraagVacatureObject?.sluitingsDatumVacature);
      this.form.controls['sluitingsDatumVacature'].setValue(sluitingsDatumVacature);


      this.form.controls['aanvraagKenmerk'].setValue(this.zoekparameters?.aanvraagKenmerk)
      this.form.controls['codeWerkEnDenkniveauMinimaal'].setValue(this.vraagVacatureObject?.codeWerkEnDenkniveauMinimaal)
      this.form.controls['indicatieLdrRegistratie'].setValue(this.vraagVacatureObject?.indicatieLdrRegistratie)
    });

    this.form = this.fb.group({
      aanvraagKenmerk: ['', Validators.required],
      codeWerkEnDenkniveauMinimaal: ['', []],
      indicatieLdrRegistratie: ['', []],
      sluitingsDatumVacature: [null, []],
    });

    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {
        this.valueChangeVacature.emit({
          codeWerkEnDenkniveauMinimaal: value.codeWerkEnDenkniveauMinimaal,
          indicatieLdrRegistratie: value.indicatieLdrRegistratie,
          sluitingsDatumVacature: dateFormatterNgbDate(value.sluitingsDatumVacature)
        })
        this.valueChangesAanvraagkenmerk.emit({
          aanvraagKenmerk: value.aanvraagKenmerk
        })
      })
    );

    this.formReady.emit(this.form);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
