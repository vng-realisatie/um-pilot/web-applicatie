import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {EMPTY, Observable, Subscription} from "rxjs";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";

@Component({
  selector: 'app-werkervaring-form',
  templateUrl: './werkervaring-form.component.html',
  styleUrls: ['./werkervaring-form.component.css']
})
export class WerkervaringFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormArray>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      if(this.vraagVacatureObject?.werkervaring) {
        this.vraagVacatureObject.werkervaring?.forEach(() => {
          this.addWerkervaring()
        })
        this.vraagVacatureObject?.werkervaring?.forEach(() => {
          this.form.controls['werkervaring'].setValue(this.vraagVacatureObject?.werkervaring)
        })
      }
    });

    this.form = this.fb.group({
      werkervaring: this.fb.array([])
    })

    this.subscription.add(
      this.form.controls.werkervaring.valueChanges.subscribe((value) => {
        this.valueChangeVacature.emit({
          werkervaring: value
        });
      })
    );

    this.formReady.emit(this.t);

  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }


  addWerkervaring() {
    this.t.push(this.fb.group({
      aantalJarenWerkzaamInBeroep: ['', [Validators.required, Validators.min(0), Validators.max(99)]]
    }));
  }

  removeWerkervaring(i: number) {
    this.t.removeAt(i);
  }

  get t() {
    return this.f.werkervaring as FormArray;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
