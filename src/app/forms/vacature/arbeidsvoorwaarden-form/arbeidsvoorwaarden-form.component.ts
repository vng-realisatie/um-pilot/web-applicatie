import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AbstractBaseFormComponent} from "../../abstract-base-form/abstract-base-form.component";
import {EMPTY, Observable, Subscription} from "rxjs";
import {dateFormatterNgbDate} from "../../../util/dateformatter";
import {MPVacature} from "../../../entities/vacature/mPVacature";
import {AanvraagVacature} from "../../../entities/vacature/aanvraagVacature";
import {MatchesRequestGemeente} from "../../../entities/shared/matchesRequestGemeente";
import {ActivatedRoute} from "@angular/router";
import {VacatureBemiddelaarService} from "../../../services/vacature-bemiddelaar.service";
import {convertToDateStruct} from "../../../util/convertToDateStruct";

@Component({
  selector: 'app-arbeidsvoorwaarden-form',
  templateUrl: './arbeidsvoorwaarden-form.component.html',
  styleUrls: ['./arbeidsvoorwaarden-form.component.css']
})
export class ArbeidsvoorwaardenFormComponent extends AbstractBaseFormComponent implements OnInit {

  aanvraagVacatures$: Observable<AanvraagVacature> = EMPTY;
  vraagVacatureId?: string | null
  vraagVacatureObject?: MPVacature

  zoekparameters?: MatchesRequestGemeente

  @Output()
  formReady = new EventEmitter<FormGroup>();

  @Output()
  valueChangeVacature = new EventEmitter<Partial<MPVacature>>();

  form: FormGroup = this.fb.group({});

  private subscription = new Subscription();

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private vacatureBemiddelaarService: VacatureBemiddelaarService) {
    super();
  }

  ngOnInit(): void {

    this.vraagVacatureId = this.route.snapshot.paramMap.get('vraagVacatureId');
    this.getVacature()

    this.aanvraagVacatures$.subscribe((data: any) => {
      this.zoekparameters = JSON.parse(data.zoekparameters)
      this.vraagVacatureObject = <MPVacature>this.zoekparameters?.vraagObject

      const datumAanvangWerkzaamheden = convertToDateStruct(this.vraagVacatureObject?.arbeidsVoorwaarden?.datumAanvangWerkzaamheden);
      this.form.controls['datumAanvangWerkzaamheden'].setValue(datumAanvangWerkzaamheden);

      const datumEindeWerkzaamheden = convertToDateStruct(this.vraagVacatureObject?.arbeidsVoorwaarden?.datumEindeWerkzaamheden);
      this.form.controls['datumEindeWerkzaamheden'].setValue(datumEindeWerkzaamheden);

      this.form.controls['salarisIndicatie'].setValue(this.vraagVacatureObject?.arbeidsVoorwaarden?.salarisIndicatie)
    });

    this.form = this.fb.group({
      datumAanvangWerkzaamheden: [null, []],
      datumEindeWerkzaamheden: [null, []],
      salarisIndicatie: ['', [Validators.maxLength(100)]]
    });

    this.subscription.add(
      this.form.valueChanges.subscribe((value) => {
        this.valueChangeVacature.emit({
          arbeidsVoorwaarden: {
            datumAanvangWerkzaamheden: dateFormatterNgbDate(value.datumAanvangWerkzaamheden),
            datumEindeWerkzaamheden: dateFormatterNgbDate(value.datumEindeWerkzaamheden),
            salarisIndicatie: value.salarisIndicatie
          }
        });
      })
    );

    this.formReady.emit(this.form);
  }

  private getVacature(): void {
    if (this.vraagVacatureId) {
      this.aanvraagVacatures$ = this.vacatureBemiddelaarService.getVraagId(this.vraagVacatureId);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
