import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {ColDef, GridOptions} from "ag-grid-community";
import {Werkzoekende} from "../../entities/werkzoekende/werkzoekende";
import {WerkzoekendeBronService} from "../../services/werkzoekende-bron.service";
import {BtnCellRendererMoreDetail} from "../../buttons/button-cell-renderer-more-details/button-cell-renderer-more-detail";
import {WerkzoekendePassService} from "../../services/werkzoekende-pass.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-werkzoekende-list',
  templateUrl: './werkzoekende-list.component.html',
  styleUrls: ['./werkzoekende-list.component.css']
})
export class WerkzoekendeListComponent implements OnInit {

  datumStamp: any
  bemiddelingsberoep: any = [];

  werkzoekenden?: Observable<Werkzoekende[]>
  gridOptions!: GridOptions;

  columnDefs: ColDef[] = [
    {headerName: "Bemiddelingsberoep", field: 'beroepsnaamOngecodeerd', valueGetter: params => {
        let temp_any:any = []
        let temp_test = params.data.bemiddelingsberoep.map((data:any) => data)

        temp_test.forEach((data:any, i:any) => {
          if (data.beroepsnaamOngecodeerd) {
            temp_any = data.beroepsnaamOngecodeerd.naamBeroepOngecodeerd
          }
        })

        this.bemiddelingsberoep = temp_any
        return this.bemiddelingsberoep
      }},
    {headerName: "Minimaal werkuren per week ", field: 'werktijden.aantalWerkurenPerWeekMinimaal', sort: 'asc', sortingOrder: ['asc', 'desc'], filter: true},
    {headerName: "Maximaal werkuren per week ", field: 'werktijden.aantalWerkurenPerWeekMaximaal', sortingOrder: ['asc', 'desc'], filter: true},
    {headerName: "Maximale reisafstand", field: 'mobiliteit.maximaleReisafstand', sortingOrder: ['asc', 'desc'], filter: true},
    {headerName: "Maximale reistijd", field: 'mobiliteit.maximaleReistijd', sortingOrder: ['asc', 'desc'], filter: true},
    {headerName: "Bemiddelingspostcode", field: 'mobiliteit.bemiddelingspostcode', sortingOrder: ['asc', 'desc'], filter: true},
    {
      headerName: "", sortable: false, cellRenderer: 'btnCellRendererMoreDetail',
      cellRendererParams: (params: any) => {
        return {
          urlPath: "/werkzoekende/id/" + params.data.idWerkzoekende,
          buttonText: "Meer detail ",
          onClick: () => this.werkzoekendePassService.setWerkzoekende(params.data)
        }
      }
    }
  ];

  constructor(private werkzoekendeService: WerkzoekendeBronService,
              private werkzoekendePassService: WerkzoekendePassService) {
  }

  ngOnInit(): void {
    this.werkzoekenden = this.werkzoekendeService.getAll();

   this.werkzoekendeService.getAll().subscribe((data:any) => {
      this.datumStamp = data[0]?.creatieDatum
    })

    this.gridOptions = <GridOptions>{
      defaultColDef: {
        editable: false,
        resizable: true,
        sortable: true
      },
      animateRows: true,
      sortingOrder: ['desc', 'asc', null],
      pagination: true,
      paginationAutoPageSize: false,
      frameworkComponents: {
        btnCellRendererMoreDetail: BtnCellRendererMoreDetail
      },
      onGridSizeChanged: (event) => {
        event.api.sizeColumnsToFit()
      },
      onGridReady: (event) => event.api.sizeColumnsToFit()
    }
  }


}
