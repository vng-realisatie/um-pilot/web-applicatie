import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {ColDef, GridOptions} from "ag-grid-community";
import {dateFormatter, dateFormatterString} from "../../util/dateformatter";
import {Vacature} from "../../entities/vacature/vacature";
import {VacatureBronService} from "../../services/vacature-bron.service";
import {VacaturePassService} from "../../services/vacature-pass.service";
import {BtnCellRendererMoreDetail} from "../../buttons/button-cell-renderer-more-details/button-cell-renderer-more-detail";

@Component({
  selector: 'app-vacature-list',
  templateUrl: './vacature-list.component.html',
  styleUrls: ['./vacature-list.component.css']
})
export class VacatureListComponent implements OnInit {

  datumStamp: any | undefined
  bemiddelingsberoep:any = []
  vacatures?: Observable<Vacature[]>
  gridOptions!: GridOptions;


  columnDefs: ColDef[] = [
    {headerName: "Bemiddelingsberoep", field: 'naamBeroepOngecodeerd', sortingOrder: ['asc', 'desc'], valueGetter: params => {
        return params.data.beroep.beroepsnaamGecodeerd.omschrijvingBeroepsnaam
      }},
    {headerName: "Naam vacature", sortingOrder: ['asc', 'desc'], field: 'naamVacature', filter: true},
    {
      headerName: "Sluitingsdatum vacature",
      field: 'sluitingsDatumVacature',
      sort: 'asc', sortingOrder: ['asc', 'desc'],
      filter: true,
      valueFormatter: dateFormatter
    },
    {
      headerName: "Datum aanvang werkzaamheden",
      field: 'arbeidsVoorwaarden.datumAanvangWerkzaamheden',
      sortingOrder: ['asc', 'desc'],
      filter: true,
      valueFormatter: (params) => {
        let data =  params.data.arbeidsVoorwaarden.datumAanvangWerkzaamheden
        if (!data) {
          return 'Geen datum';
        }
        let datumString = dateFormatterString(data)
        return ""+ datumString
    }},
    {
      headerName: "Datum einde werkzaamheden",
      valueFormatter: (params) => {
        let data = params.data.arbeidsVoorwaarden.datumEindeWerkzaamheden
        if (!data) {
          return 'Geen datum';
         }
        let datumString = dateFormatterString(data)
        return ""+ datumString
      },
        sortingOrder: ['asc', 'desc'],
      filter: true,
    },
    {headerName: "Salaris indicatie", field: 'arbeidsVoorwaarden.salarisIndicatie', sortingOrder: ['asc', 'desc'], filter: true},
    {
      headerName: "", sortable: false, cellRenderer: 'btnCellRendererMoreDetail', suppressSizeToFit: true,
      cellRendererParams: (params: any) => {
        return {
          urlPath: "/vacature/id/" + params.data.idVacature,
          buttonText: "Meer detail",
          onClick: () => this.vacaturePassService.setVacature(params.data)
        }
      }
    }
  ];

  constructor(private vacatureService: VacatureBronService,
              private vacaturePassService: VacaturePassService) {
  }

  ngOnInit(): void {
    this.vacatures = this.vacatureService.getAll();

    this.vacatureService.getAll().subscribe((data:any) => {
      this.datumStamp = data[0]?.creatieDatum
    })

    this.gridOptions = <GridOptions>{
      defaultColDef: {
        editable: false,
        resizable: true,
        sortable: true
      },
      animateRows: true,
      sortingOrder: ['desc', 'asc', null],
      pagination: true,
      paginationAutoPageSize: true,
      frameworkComponents: {
        btnCellRendererMoreDetail: BtnCellRendererMoreDetail
      },
      onGridSizeChanged: (event) => {
        event.api.sizeColumnsToFit()
      },
      onGridReady: (event) => {
        event.api.sizeColumnsToFit();
      }
    }
  }

}
