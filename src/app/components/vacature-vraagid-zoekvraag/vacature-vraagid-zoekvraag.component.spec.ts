import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VacatureVraagidZoekvraagComponent } from './vacature-vraagid-zoekvraag.component';

describe('VacatureVraagidZoekvraagComponent', () => {
  let component: VacatureVraagidZoekvraagComponent;
  let fixture: ComponentFixture<VacatureVraagidZoekvraagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VacatureVraagidZoekvraagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VacatureVraagidZoekvraagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
