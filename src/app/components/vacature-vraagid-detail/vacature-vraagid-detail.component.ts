import {Component, OnInit} from '@angular/core';
import {EMPTY, Observable} from "rxjs";
import {AanvraagVacature} from "../../entities/vacature/aanvraagVacature";
import {ColDef, GridApi, GridOptions} from "ag-grid-community";
import {dateFormatter, dateFormatterString} from "../../util/dateformatter";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {VacatureBemiddelaarService} from "../../services/vacature-bemiddelaar.service";
import {BtnCellRendererMoreDetail} from "../../buttons/button-cell-renderer-more-details/button-cell-renderer-more-detail";
import {MPVacatureMatch} from "../../entities/vacature/mPVacatureMatch";
import {VacaturePassService} from "../../services/vacature-pass.service";
import {
  ButtonCellRendererFetched
} from "../../buttons/button-cell-renderer-fetched/button-cell-renderer-fetched.component";

@Component({
  selector: 'app-vacature-vraagid-detail',
  templateUrl: './vacature-vraagid-detail.component.html',
  styleUrls: ['./vacature-vraagid-detail.component.css']
})
export class VacatureVraagidDetailComponent implements OnInit {

  aanvraagVacature$: Observable<AanvraagVacature> = EMPTY;
  vraagId?: string | null;

  gridApi!: GridApi;
  gridOptions!: GridOptions;
  columnDefs: ColDef[] = [
    {headerName: "Beroep Vacature", field: 'beroep.beroepsnaamGecodeerd.omschrijvingBeroepsnaam', sortingOrder: ['asc', 'desc'],  filter: true,  valueFormatter: function(params) {
        if (params.value === null || params.value === undefined) {
          return params.data.beroep.beroepsnaamGecodeerd.codeBeroepsnaam;
        } else {
          return params.value;
        }
      }
    },
    {headerName: "Sluitingsdatum", field: 'sluitingsDatumVacature', sort: 'asc', sortingOrder: ['asc', 'desc'], filter: true, valueFormatter: dateFormatter},
    {
      headerName: "Datum aanvang werkzaamheden",
      sortingOrder: ['asc', 'desc'],
      filter: true,
      valueFormatter: (params) => {
        let data =  params.data.arbeidsVoorwaarden.datumAanvangWerkzaamheden
        if (!data) {
          return 'Geen datum';
        }
        let datumString = dateFormatterString(data)
        return ""+ datumString
      }
    },
    {
      headerName: "Datum einde werkzaamheden",
      sortingOrder: ['asc', 'desc'],
      filter: true,
      valueFormatter: (params) => {
        let data =  params.data.arbeidsVoorwaarden.datumEindeWerkzaamheden
        if (!data) {
          return 'Geen datum';
        }
        let datumString = dateFormatterString(data)
        return ""+ datumString
      }
    },
    {
      headerName: "Detail aangevraagd", sortingOrder: ['asc', 'desc'], field: "fetchedDetail", cellRenderer: 'btnCellRendererFetched'
    },
    {
      headerName: "", sortable: false, cellRenderer: 'btnCellRendererMoreDetail',
      cellRendererParams: (params: any) => {
        return {
          urlPath: "/vacature/" + this.vraagId + "/" + params.data.vumID,
          buttonText: "Meer detail",
          onClick: () => this.vacaturePassService.setVacature(params.data)
        }
      }
    }
  ];

  constructor(private route: ActivatedRoute,
              private location: Location,
              private vacatureService: VacatureBemiddelaarService,
              private vacaturePassService: VacaturePassService) {

  }

  ngOnInit(): void {
    this.vraagId = this.route.snapshot.paramMap.get('vraagId');
    this.getVacature();

    this.gridOptions = <GridOptions>{
      defaultColDef: {
        editable: false,
        sortable: true
      },
      animateRows: true,
      sortingOrder: ['desc', 'asc', null],
      pagination: true,
      paginationAutoPageSize: true,
      frameworkComponents: {
        btnCellRendererMoreDetail: BtnCellRendererMoreDetail,
        btnCellRendererFetched: ButtonCellRendererFetched
      },
      onGridSizeChanged: (event) => {
        event.api.sizeColumnsToFit()
      },
      getRowNodeId: (data: MPVacatureMatch) => {
        return data.vumID;
      },
      onGridReady: (event) => {
        this.gridApi = event.api;
        event.api.sizeColumnsToFit()
      }
    }
  }

  private getVacature(): void {
    if (this.vraagId) {
      this.aanvraagVacature$ = this.vacatureService.getVraagId(this.vraagId);
    }
  }

  goBack(): void {
    this.location.back();
  }

  refresh(): void {
    this.aanvraagVacature$.subscribe(aanvraagVacature =>
      this.gridApi.setRowData(aanvraagVacature.vacatures)
    );
  }

}
