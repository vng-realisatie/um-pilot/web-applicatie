import {Component, OnInit} from '@angular/core';
import {AuthService} from 'src/app/services/auth.service';
import jwt_decode from "jwt-decode";
import {OAuthService} from "angular-oauth2-oidc";


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  hasRole?:any

  constructor(
    private authService: AuthService,
    private oauthService: OAuthService
  ) {
  }

  ngOnInit(): void {
    setTimeout(this.anAsyncMethod, 250)
  }



  anAsyncMethod = async () => {
      let token = this.oauthService.getAccessToken()
      let decode = jwt_decode(token)
      let decodeMapper:any = [decode]
      let dataArray:any = []
      decodeMapper.map((data:any) => data.roles.forEach((data: any) => {dataArray.push(data)}))
      this.hasRole = dataArray
  }

  uploadVacatures() {
    try{
      return this.hasRole.indexOf('upload-vacatures') > -1;
    } catch (e) {
      return false
    }
  }

  zoekenVacatures()  {
    try{
      return this.hasRole.indexOf('zoeken-vacatures') > -1;
    } catch (e) {
      return false
    }
  }

  uploadWerkzoekende(){
    try{
      return this.hasRole.indexOf('upload-werkzoekende') > -1;
    } catch (e){
      return false
    }
  }

  zoekenWerkzoekende()  {
    try {
      return this.hasRole.indexOf('zoeken-werkzoekende') > -1;
    } catch (e) {
      return false
    }
  }


  rapportage()  {
    try {
      return this.hasRole.indexOf('rapportage') > -1;
    } catch (e) {
      return false
    }
  }

  logout() {
    this.authService.logout();
  }
}
