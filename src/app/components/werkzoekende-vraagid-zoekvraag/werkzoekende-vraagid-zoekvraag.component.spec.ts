import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WerkzoekendeVraagidZoekvraagComponent } from './werkzoekende-vraagid-zoekvraag.component';

describe('WerkzoekendeVraagidZoekvraagComponent', () => {
  let component: WerkzoekendeVraagidZoekvraagComponent;
  let fixture: ComponentFixture<WerkzoekendeVraagidZoekvraagComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WerkzoekendeVraagidZoekvraagComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WerkzoekendeVraagidZoekvraagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
