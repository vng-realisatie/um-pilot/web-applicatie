import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {ColDef, GridOptions} from "ag-grid-community";
import {dateFormatter} from "../../util/dateformatter";
import {BtnCellRenderer} from "../../buttons/button-cell-renderer/button-cell-renderer.component";
import {WerkzoekendeBemiddelaarService} from "../../services/werkzoekende-bemiddelaar.service";
import {AanvraagWerkzoekende} from "../../entities/werkzoekende/aanvraagWerkzoekende";

@Component({
  selector: 'app-werkzoekende-vraagid-list',
  templateUrl: './werkzoekende-vraagid-list.component.html',
  styleUrls: ['./werkzoekende-vraagid-list.component.css']
})
export class WerkzoekendeVraagidListComponent implements OnInit {

  aanvraagWerkzoekenden?: Observable<AanvraagWerkzoekende[]>
  gridOptions!: GridOptions;

  columnDefs: ColDef[] = [
    {headerName: "Kenmerk zoekvraag", field: 'aanvraagKenmerk',  sort: null , sortingOrder: ['asc', 'desc', null], filter: true},
    {headerName: "Datum van zoekvraag", field: 'creatieDatum', sort: 'asc', sortingOrder: ['asc', 'desc'], filter: true, valueFormatter: dateFormatter},
    {headerName: "Aantal resterende detailaanvragen", field: 'timesAccessLeft', sortingOrder: ['asc', 'desc'],  filter: true},
    {
      headerName: "", sortable: false, cellRenderer: 'btnCellRenderer',
      cellRendererParams:(params: any) => {
        return {
          urlPath: "/werkzoekende/vraagid-detail/" + params.data.vraagId,
          buttonText: "Werkzoekenden"
        }
      }
    },
    {
      headerName: "", sortable: false, cellRenderer: 'btnCellRenderer',
      cellRendererParams:(params: any) => {
        return {
          urlPath: "/werkzoekende/vraagid-zoekvraag/" + params.data.vraagId,
          buttonText: "Zoekvraag"
        }
      }
    }
  ];

  constructor(private werkzoekendeService: WerkzoekendeBemiddelaarService) {
  }

  ngOnInit(): void {
    this.aanvraagWerkzoekenden = this.werkzoekendeService.getAllVraagId();

    this.gridOptions = <GridOptions>{
      defaultColDef: {
        editable: false,
        sortable: true
      },
      animateRows: true,
      sortingOrder: ['desc', 'asc', null],
      pagination: true,
      paginationAutoPageSize: true,
      frameworkComponents: {
        btnCellRenderer: BtnCellRenderer
      },
      onGridSizeChanged: (event) => {
        event.api.sizeColumnsToFit()
      },
      onGridReady: (event) => event.api.sizeColumnsToFit()
    }
  }

}
