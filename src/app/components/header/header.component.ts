import {Component, OnInit} from '@angular/core';
import {AuthService} from 'src/app/services/auth.service';
import {Subscription} from "rxjs";
import {OinService} from "../../services/oin.service";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  wait = false
  selectedGemeente!: string;
  oin?: string;
  roles?: any = []

  subscription?: Subscription

  constructor(private authService: AuthService,
    private oinService: OinService,) {

  }

   ngOnInit(): void {
    this.subscription = this.authService.isDoneLoading$.subscribe(x => this.oin = this.authService.getOin());
    this.subscription = this.authService.isDoneLoading$.subscribe(x => this.roles = this.authService.getRoles())

     setTimeout(this.selectGemeente, 250)
  }

  selectGemeente = async () => {
    this.selectedGemeente = this.oinService.getGemeente(this.authService.getOin())
    this.wait = true
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }



}
