import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {AuthConfig, OAuthService} from 'angular-oauth2-oidc';
import {BehaviorSubject, ReplaySubject} from 'rxjs';
import { environment } from 'src/environments/environment';
import jwt_decode from "jwt-decode";

@Injectable({providedIn: 'root'})
export class AuthService {

  private isAuthenticatedSubject$ = new BehaviorSubject<boolean>(false);
  public isAuthenticated$ = this.isAuthenticatedSubject$.asObservable();

  private isDoneLoadingSubject$ = new ReplaySubject<boolean>();
  public isDoneLoading$ = this.isDoneLoadingSubject$.asObservable();

  private oin?: string;
  roles: any;

  strictDiscoveryDocumentValidationValue = environment.strictDiscoveryDocumentValidation;
  strictDiscoveryDocumentValidationBool = JSON.parse(this.strictDiscoveryDocumentValidationValue);

  requireHttpsValue = environment.requireHttps;
  requireHttpsBool = JSON.parse(this.requireHttpsValue);


  authConfig: AuthConfig = {
    issuer: environment.jwtIssuerUri,
    redirectUri: window.location.origin,
    clientId: environment.clientId,
    scope: environment.scope,
    responseType: 'code',
    requireHttps: this.requireHttpsBool,
    strictDiscoveryDocumentValidation: this.strictDiscoveryDocumentValidationBool,
    // at_hash is not present in JWT token
    disableAtHashCheck: true,
    showDebugInformation: true
  }


  constructor(
    private oauthService: OAuthService,
    private router: Router,
  ) {
    this.configure();
  }

  public login(targetUrl?: string)  {
    this.oauthService.initLoginFlow(targetUrl || this.router.url);
  }


  public logout() {
    this.oauthService.logOut();
  }

  public refresh() {
    this.oauthService.silentRefresh();
  }

  public hasValidToken() {
    return this.oauthService.hasValidAccessToken();
  }

  private configure() {
    this.oauthService.configure(this.authConfig);
    this.oauthService.setupAutomaticSilentRefresh();
    this.oauthService.events.subscribe(_ => {
      this.isAuthenticatedSubject$.next(this.oauthService.hasValidAccessToken());
      const profile = this.oauthService.getIdentityClaims();
      if (profile) {
        let token = this.oauthService.getAccessToken()
        let decode = jwt_decode(token)
        let decodeMapper:any = [decode]
        let dataArray:any = []
        decodeMapper.map((data:any) => data.roles.forEach((data: any) => {dataArray.push(data)}))
        this.roles = dataArray
        //obtain oin after having received valid token
        this.oin = (profile as any)['oin'];
      }
    });
    this.oauthService.loadDiscoveryDocumentAndLogin()
      .then(() => this.isDoneLoadingSubject$.next(true))
      .catch(() => this.isDoneLoadingSubject$.next(true));
  }

  public getOin(): string {
    if (this.oin) {
      return this.oin;
    } else {
      return "undefined";
    }
  }

  public getRoles(): any {
    if (this.roles) {
      return this.roles
    } else {
      return "undefined"
    }

  }
}
