import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class OinService {

  private appConfig: any;

  constructor(private notificationService: NotificationService, private http: HttpClient) {
  }

  getAppConfig() {
    return this.http.get('/assets/app-config.json').toPromise().then(config => {
      this.appConfig = config;
    });
  }


  getGemeente(oin: string): string {
    if (oin === '') {
      console.log('oin empty');
    }
    if (oin === '') {
      console.log('oin empty');
    }
    if (this.appConfig.organisaties == undefined) {
      console.log('appConfig undefined');
      return '';
    }

    const organisation = this.appConfig.organisaties.find(
      (i: any) => i.oin === oin
    );
    if (organisation == undefined) {
      console.log('org undefined');
      return '';
    }
    const result = organisation.naam;
    console.log('org ' + organisation.naam + " " + organisation.oin);
    if (result) {
      return result;
    } else {
      this.notificationService.showError('Gemeente not found');
      return '';
    }
  }
}
