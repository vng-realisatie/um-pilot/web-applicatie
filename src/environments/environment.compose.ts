//@ts-nocheck
export const environment = {
  production: false,
  gatewayUrl: window['env']['gatewayUrl'] || "http://localhost:8081", // kong gateway
  kibanaUrl: window['env']['kibanaUrl'] || "http://localhost:8082", // kibana
  jwtIssuerUri: window['env']['jwtIssuerUri'] || "http://host.docker.internal:8083/auth/realms/poc-vng-develop-realm",// identity server
  strictDiscoveryDocumentValidation: window['env']['strictDiscoveryDocumentValidation'] || true,
  clientId: window['env']['clientId'] || "poc-vng-frontend",
  scope: window['env']['scope'] || "openid profile email offline_access",
  requireHttps: window['env']['requireHttps'] || false,
  frontEndVersion: window['env']['frontEndVersion'] || "UM0.10.6",
  helpDeskTelefoonNr: window['env']['helpDeskTelefoonNr'] || "06 123456789"
};




//Server URLS
// gatewayUrl: "https://um-api.testdorp.nl", // kong gateway
// kibanaUrl: "http://localhost:8082", // kibana
// keycloak: "https://um-admin.testdorp.nl/auth/realms/poc-vng-realm" // keycloak auth

//   gatewayUrl: "https://demo-umapi.testdorp.nl", // kong gateway
//   kibanaUrl: "http://localhost:8082", // kibana
//   keycloak: "https://demo-umadmin.testdorp.nl/auth/realms/poc-vng-realm" // keycloak auth

//   gatewayUrl: "https://acc1-umapi.testdorp.nl", // kong gateway
//   kibanaUrl: "http://localhost:8082", // kibana
//   keycloak: "https://acc1-umadmin.testdorp.nl/auth/realms/poc-vng-realm" // keycloak auth

