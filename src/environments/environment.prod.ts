export const environment = {
  production: true,
  gatewayUrl: "http://localhost:8000", // kong
  kibanaUrl: "http://localhost:5601", // kibana
  jwtIssuerUri: "http://localhost:8089/auth/realms/poc-vng-realm" // // identity server
};
