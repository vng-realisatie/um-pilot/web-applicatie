(function (window) {
  window['env'] = window['env'] || {};

  // Environment variables
  window['env']['gatewayUrl'] = '${GATEWAY_URL}';
  window['env']['kibanaUrl'] = '${KIBANA_URL}';
  window['env']['jwtIssuerUri'] = '${JWT_ISSUER_URI}';
  window['env']['strictDiscoveryDocumentValidation'] = '${STRICT_DISCOVERY_DOCUMENT_VALIDATION}';
  window['env']['clientId'] = '${CLIENT_ID}';
  window['env']['scope'] = '${SCOPE}';
  window['env']['requireHttps'] = '${REQUIRE_HTTPS}';
  window['env']['frontEndVersion'] = '${FRONT_END_VERSION}';
  window['env']['helpDeskTelefoonNr'] = '${FRONT_END_TELEFOONNR}';
})(this);
