(function (window) {
  window['env'] = window['env'] || {};

  // Environment variables
  window['env']['gatewayUrl'] = "http://localhost:8081";
  window['env']['kibanaUrl'] = "http://localhost:8082";
  window['env']['jwtIssuerUri'] = "http://host.docker.internal:8083/auth/realms/poc-vng-develop-realm";
  window['env']['strictDiscoveryDocumentValidation'] = true;
  window['env']['clientId'] = "poc-vng-frontend";
  window['env']['scope'] = "openid profile email offline_access";
  window['env']['requireHttps'] = false;
  window['env']['frontEndVersion'] = "UM0.10.6";
  window['env']['helpDeskTelefoonNr'] = "06 123456789";
})(this);
