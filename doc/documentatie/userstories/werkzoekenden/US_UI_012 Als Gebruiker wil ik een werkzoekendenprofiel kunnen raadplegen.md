# User Story : US_UI_012 Als Gebruiker wil ik een werkzoekendenprofiel kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** een werkzoekendenprofiel kunnen raadplegen
**zodat ik** het profiel kan beoordelen

### Functioneel
Tijdens het bemiddelen naar werk moeten bemiddelaars profielen kunnen beoordelen.
Hiertoe moeten zij een profiel kunnen aanwijzen en de detailgegevens kunnen bekijken.
In dit overzicht worden alle gegevens van het profiel getoond. 
Dit is een samenvoeging van de gegevens die beschreven zijn in de sub stories (zie hieronder)

### User Stories
De detailsgegevens zijn opgedeeld in een aantal secties die elk in een eigen userstory beschrven worden.
Het gaat hierbij om :
[Algemene gegevens](US_UI_012A%20Als%20Gebruiker%20wil%20ik%20de%20algemene%20gegevens%20van%20een%20werkzoekendenprofiel%20kunnen%20raadplegen.md)
[Contactgegevens](US_UI_012B_Als%20Gebruiker%20wil%20ik%20de%20Contactgegevens%20van%20een%20werkzoekendenprofiel%20kunnen%20raadplegen.md)
[Werkervaring](US_UI_012C_Als%20Gebruiker%20wil%20ik%20de%20Werkervaring%20van%20een%20werkzoekendenprofiel%20kunnen%20raadplegen.md)
[Opleiding](US_UI_012D%20Als%20Gebruiker%20wil%20ik%20de%20Opleidingen%20van%20een%20werkzoekendenprofiel%20kunnen%20raadplegen.md)
[Kwalificatie](US_UI_012E%20Als%20Gebruiker%20wil%20ik%20de%20Kwalificaties%20van%20een%20werkzoekendenprofiel%20kunnen%20raadplegen.md)

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen werkzoekende profiel vanaf overzicht geuploade werkzoekenden*
**Gegeven** de Gebruiker heeft een overzicht van werkzoekendenn  
**Wanneer** de Gebruiker de detail gegevens opvraagt
**En** het overzicht bekijkt
**Dan** krijgt de Gebruiker alle bekende gegevens over deze werkzoekende te zien
