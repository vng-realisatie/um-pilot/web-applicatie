# User Story : US_UI_005 Als profielverantwoordelijke wil ik een overzicht van de geuploade werkzoekendenprofielen kunnen raadplegen
versie 0.10

_Versionering_

| versie | datum         | opmerking |
|--------|---------------|-----------|
| 0.10   | februari 2023 | Initieel  |

**Als** profielverantwoordelijke
**wil ik** een overzicht van de geuploade werkzoekendenprofielen kunnen raadplegen
**zodat ik** weet welke profielen zijn opgenomen in UM

### Functioneel
Nadat een upload is gedaan wil de gebruiker de geuploade werkzoekendenprofielen kunnen raadplegen
Deze userstory beschrijft de terugkoppeling van de succesvolle verwerking aan de gebruiker.
De gebruiker krijgt een overzicht waarop de datum/tijd van de upload en de hoeveelheid aangeboden en correct verwerkte records is te zien.


### User Stories
nvt

#### Afhankelijkheden
Zie parent

### Technische Documentatie
nvt

### High level Acceptatiecriteria

*Precondities :*
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te uploaden

*Feature: Complete verwerking*
**Gegeven** de Gebruiker heeft een bestand werkzoekendeprofielen ter verwerking aangeboden  
**Wanneer** de verwerking is afgerond  
**Dan** zijn de reeds bestaande profielen verwijderd  
**En** zijn de nieuwe werkzoekendeprofielen aangeboden aan VUM  
**En** is op het overzicht de datum/tijd upload, het totaal aantal records en het aantal verwerkte records te raadplegen
