# User Story : US_UI_012A Als Gebruiker wil ik de algemene gegevens van een werkzoekendenprofiel kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** de algemene gegevens van een werkzoekendenprofiel kunnen raadplegen
**zodat ik** een algemeen beeld krijg van het profiel

### Functioneel
Tijdens het bemiddelen naar werk moeten bemiddelaars profielen kunnen beoordelen.
Hiertoe moeten zij een profiel kunnen aanwijzen en de detailgegevens bekijken.
In dit overzicht worden de algemene gegevens van het profiel getoond. 
Het gaat hierbij om :
- Algemeen
  - Indicatie LDR registratie
  - Indicatie beschikbaarheid contactgegevens
  - Persoonlijke presentatie
- Werktijden
  - Aantal werkuren per week minimaal
  - Aantal werkuren per week maximaal
  - Indicatie kantoortijden
- Flexibiliteit
  - Code regiostraal
  - Indicatie onregelmatig werk of ploegendienst
  - Datum aanvang beschikbaar voor werk
  - Datum einde beschikbaar voor werk
- Mobiliteit
  - Bemiddelingspostcode
  - Maximale reisafstand (km)
  - Maximale reistijd (min)
- Vervoermiddel
  - Vervoermiddel
  - Vervoermiddel beschikbaar voor uitvoering werk
  - Vervoermiddel beschikbaar voor woon-werkverkeer
- Voorkeursland
  - Voorkeursland
- Contractvorm
- Eis aan werk
  - Indicatie aanpassing werkomgeving
  - Indicatie begeleiding
  - Indicatie werkvariatie

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen werkzoekende profiel vanaf overzicht geuploade werkzoekenden*
**Gegeven** de Gebruiker heeft een overzicht van werkzoekendenn  
**Wanneer** de Gebruiker de detail gegevens opvraagt
**Dan** krijgt de Gebruiker alle algemene gegevens over deze werkzoekende te zien

*Scenario: Raadplegen werkzoekende profiel vanaf overzicht gevonden werkzoekenden*
**Gegeven** de Gebruiker heeft een overzicht van gevonden werkzoekenden  
**Wanneer** de Gebruiker de detail gegevens van een werkzoekenden opvraagt
**Dan** krijgt de Gebruiker alle algemene gegevens over deze werkzoekende te zien
