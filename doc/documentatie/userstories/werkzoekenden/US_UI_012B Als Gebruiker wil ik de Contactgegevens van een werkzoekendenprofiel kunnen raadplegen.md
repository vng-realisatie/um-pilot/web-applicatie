# User Story : US_UI_012B_Als Gebruiker wil ik de Contactgegevens van een werkzoekendenprofiel kunnen raadplegen
versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** de Contactgegevens van een werkzoekendenprofiel kunnen raadplegen
**zodat ik** contact op kan nemen 

### Functioneel
In het geval van een match op een vacature kan via een van de contacgevens de werkzoekende benaderd worden

- Contactgegevens
  - Emailadres  
  - Telefoonnummer  
  - Naam contactpersoon
  - Telefoonnummer contactpersoon
  - Emailadres contactpersoon

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen werkzoekende profiel vanaf overzicht geuploade werkzoekenden*
**Gegeven** de Gebruiker heeft een overzicht van werkzoekenden  
**Wanneer** de Gebruiker de detail gegevens opvraagt
**Dan** krijgt de Gebruiker de contactgegevens van deze werkzoekende te zien

*Scenario: Raadplegen werkzoekende detailprofiel vanaf overzicht gevonden werkzoekenden*
**Gegeven** de Gebruiker bekijkt de detailsgegevens van gevonden werkzoekenden  
**Wanneer** de Gebruiker de gegevens van een werkzoekenden opvraagt
**En** van de werkzoekende meer detailgegevens opvraagt
**Dan** krijgt de Gebruiker de contactgegevens van deze werkzoekende te zien

*Scenario: Raadplegen werkzoekende profiel vanaf overzicht gevonden werkzoekenden*
**Gegeven** de Gebruiker bekijkt de detailsgegevens van gevonden werkzoekenden  
**Wanneer** de Gebruiker de gegevens van een werkzoekenden opvraagt
**Dan** krijgt de Gebruiker de contactgegevens van deze werkzoekende niet te zien
