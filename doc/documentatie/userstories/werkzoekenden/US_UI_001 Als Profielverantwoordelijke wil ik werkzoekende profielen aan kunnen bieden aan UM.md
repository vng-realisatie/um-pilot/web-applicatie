# User Story : US_UI_001 Als Profielverantwoordelijke wil ik werkzoekende profielen aan kunnen bieden aan UM

versie 0.10

_Versionering_

| versie | datum         | opmerking |
|--------|---------------|-----------|
| 0.10   | februari 2023 | Initieel  |


**Als** Als Profielverantwoordelijke  
**Wil ik** werkzoekende profielen aan kunnen bieden aan UM
**zodat** deze profielen vindbaar worden voor bemiddeling

Functioneel
-----------
Om matching mogelijk te maken moet een gebruiker in staat zijn om werkzoekendeprofielen op te kunnen nemen in UM.
Een van de mogelijkheden die aangeboden wordt is het importeren van een json object met daarin de gegevens van de profielen.
De gegevens worden batch gewijs opgevoerd. 
Deze oplossing biedt geen mogelijkheid tot het opvoeren van individuele profielen.

Zie verder :  
[US_UI_001B](US_UI_001B%20Als%20Profielverantwoordelijke%20wil%20ik%20gegevens%20alleen%20met%20toestemming%20van%20de%20werkzoekende%20worden%20opgenomen.md)

Technisch
-------------
nvt

Acceptatiecriteria
------------------------
*Feature: upload valide batch werkzoekenden*  
**Gegeven** een gebruiker is ingelogd  
**En** de gebruiker heeft rechten om werkzoekendeprofielen te beheren  
**Wanneer** de gebruiker een valide json upload  
**En** de gebruiker aangeeft dat de werkzoekende toestemming hebben gegeven  
**En** de gebruiker het bestand indient  
**Dan** zijn de profielen toegevoegd aan UM   
**En** wordt de gebruiker doorgeleid naar het overzicht geuploade werkzoekenden  

*Feature: upload niet valide batch werkzoekenden*  
**Gegeven** een gebruiker is ingelogd  
**En** de gebruiker heeft rechten om werkzoekendeprofielen te beheren  
**Wanneer** de gebruiker een niet valide json upload  
**En** de gebruiker aangeeft dat de werkzoekende toestemming hebben gegeven  
**En** de gebruiker het bestand indient  
**Dan** zijn de profielen niet toegevoegd aan UM  
**En** zijn de bestaande profielen niet verwijderd uit UM  
**En** krijgt de gebruiker een foutmelding  


