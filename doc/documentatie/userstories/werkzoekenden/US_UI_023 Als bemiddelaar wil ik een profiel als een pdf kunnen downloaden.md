# Feature : US_UI_023 Als bemiddelaar wil ik een profiel als pdf kunnen downloaden

_Versionering_

2qkz4zc7s2

| versie | datum        | opmerking                             |
|--------|--------------|---------------------------------------|
| 0.1    | januari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** een profiel als pdf kunnen downloaden  
**zodat** ik een offline profiel tot mijn beschikking heb

### Functioneel
Todo

#### Gerelateerde trello kaartjes ####
22-1 Als gemeente kan ik zowel de antwoorden als detailantwoorden downloaden in de UM GUI, om deze te exporteren uit het systeem.
22-2 Vormgeving exportbestand

#### User Stories
- TODO (zie onder)

### Technische Documentatie

### Acceptatiecriteria

*Dit zijn high-level acceptatie criteria die in de onderliggende User Stories verder worden uitgwerkt*

*Scenario : Downloaden pdf werkzoekendeprofiel*  
**Gegeven** de bemiddelaar raapleegt het werkzoekende profiel in detail 
**Wanneer** de bemiddelaar het profiel downloadt  
**Dan** wordt een pdf met daarop de profiel gegevens beschikbaar gesteld
