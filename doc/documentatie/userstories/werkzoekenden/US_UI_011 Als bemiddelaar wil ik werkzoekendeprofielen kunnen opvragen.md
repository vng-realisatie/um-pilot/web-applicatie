# User Story : US_UI_011 Als bemiddelaar wil ik werkzoekendeprofielen kunnen opvragen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** werkzoekendenprofielen kunnen opvragen
**zodat ik** de profielen kan vinden bij een vacature

### Functioneel
Tijdens het bemiddelen naar werk moeten bemiddelaars werkzoekenden op basis van kenmerken kunnen vinden.
Deze functionaliteit filtert de werkzoekendeprofielen op basis van geregistreerde gegevens bij deze profielen.
De gegevens waarop gefilterd kan worden zijn : 
- Indicatie LDR registratie
- Werktijden
- Flexibiliteit
- Mobiliteit
- Code werk en denk niveau werkzoekende
- Vakvaardigheid
- Gedragscompetentie
- Rijbewijs
- Cursus
- Opleiding
- Taalbeheersing
- Werkervaring
- Vervoermiddel
- Voorkeursland
- Contractvorm
- Sector
- Bemiddelingsberoep

Wanneer er gefilterd worden alleen die profielen aangeboden die voldoen aan de opgegeven criteria. 
Wanneer er van een bepaalde categorie meerdere worden opgegeven dan geldt dat het profiel aan een van deze criteria dient te voldoen ('of')
Wanneer er gefilterd wordt op een combinatie van criteria dan dienen de aangeboden profielen te voldoen aan de combinatie van die criteria.
Maw dit is een 'En'-relatie en geen 'Of'.

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Indienen verzoek voor profielen*
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen ongeldig verzoek*
**Gegeven** de Gebruiker geeft geen zoektitel  
**Of** geen Bemiddelingscode  
**Of** een regiostraal op  
**Wanneer** de Gebruiker het verzoek wil indienen  
**Dan** krijgt de Gebruiker niet de mogelijkheid een verzoek in te dienen  

*Scenario: Indienen verzoek voor profielen met Indicatie LDR registratie*
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft een 'Indicatie LDR registratie' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan de 'Indicatie LDR registratie'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met Werktijden*
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft 'Werktijden' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan de 'Werktijden'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina   

*Scenario: Indienen verzoek voor profielen met Flexibiliteit*
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft 'Flexibiliteit' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan de 'Flexibiliteit'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met Mobiliteit*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft 'Mobiliteit' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan de 'Mobiliteit'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Code werk en denk niveau werkzoekende'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft 'Code werk en denk niveau werkzoekende' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan de 'Code werk en denk niveau werkzoekende'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Vakvaardigheid'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft een of meerdere 'Vakvaardigheden' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Vakvaardigheden'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Gedragscompetentie'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft een of meerdere 'Competenties' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen   
**En** voldoen aan een van de 'Competenties'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Rijbewijs'*
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op  
**En** de Gebruiker geeft een of meerdere 'Rijbewijzen' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen   
**En** voldoen aan een van de 'Rijbewijzen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Cursus'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Cursussen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Cursussen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Opleiding'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Opleidingen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Opleidingen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Taalbeheersing'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Taalbeheersingen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Taalbeheersingen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Werkervaring'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Werkervaringen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Werkervaringen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Vervoermiddel'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Vervoermiddelen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Vervoermiddelen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Voorkeursland'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Voorkeurslanden' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Voorkeurslanden'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor profielen met 'Contractvorm'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Contractvormen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Contractvormen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor profielen met 'Sector'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Sectoren' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Sectoren'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor profielen met 'Bemiddelingsberoep'*  
**Gegeven** de Gebruiker geeft een zoektitel, een Bemiddelingscode en een regiostraal op   
**En** de Gebruiker geeft een of meerdere 'Bemiddelingsberoepen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van profielen die binnen de opgegeven regiostraal vallen  
**En** voldoen aan een van de 'Bemiddelingsberoepen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

