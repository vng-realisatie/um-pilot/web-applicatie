# User Story : US_UI_012E Als Gebruiker wil ik de Kwalificaties van een werkzoekendenprofiel kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** Opleidingen van een Kwalificaties kunnen raadplegen
**zodat ik** ik kan beoordelen of de Kwalificaties aansluiten op een vacature 

### Functioneel
Bij het matchen op een vacature is de kwalificatie een van de criteria waarop de werkzoekende gevonden kan worden

- Code werk en denk niveau werkzoekende
- Vakvaardigheid
  - Omschrijving vakvaardigheid  
- Gedragscompetentie
  - Code gedragscompetentie 
  - Beheersing gedragscompetentie
  - Omschrijving gedragscompetentie
- Taalbeheersing
  - Code taal
  - Taalbeheersing mondeling
  - Taalbeheersing schriftelijk
  - Taalbeheersing lezen
  - Taalbeheersing luisteren  
  - 
### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen werkzoekende profiel vanaf overzicht geuploade werkzoekenden*
**Gegeven** de Gebruiker bekijkt de detail gegevens van een werkzoekende
**Wanneer** de Gebruiker de Kwalificaties opvraagt
**Dan** krijgt de Gebruiker de Kwalificaties van deze werkzoekende te zien

*Scenario: Raadplegen werkzoekende detailprofiel vanaf overzicht gevonden werkzoekenden*
**Gegeven** de Gebruiker bekijkt detail gegevens van een gevonden werkzoekende  
**Wanneer** de Gebruiker de Kwalificaties opvraagt
**Dan** krijgt de Gebruiker de Kwalificaties van deze werkzoekende te zien
