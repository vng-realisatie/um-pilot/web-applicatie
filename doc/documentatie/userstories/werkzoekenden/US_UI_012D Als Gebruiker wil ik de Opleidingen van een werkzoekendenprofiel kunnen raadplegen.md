# User Story : US_UI_012D Als Gebruiker wil ik de Opleidingen van een werkzoekendenprofiel kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** Opleidingen van een werkzoekendenprofiel kunnen raadplegen
**zodat ik** ik kan beoordelen of de opleiding aanlsuit op een vacature 

### Functioneel
Bij het matchen op een vacature is de opleiding een van de belangrijkste criteria waarop de werkzoekende gevonden kan worden

- Rijbewijs
  - Code soort rijbewijs
- Opleiding
  - Naam opleiding  
  - Omschrijving opleiding 
  - Diploma behaald
  - Datum diploma
  - Naam opleidingsinstituut
  - Datum aanvang volgen opleiding
  - Datum einde volgen opleiding
  - Code status opleiding
- Cursus
  - Naam cursus
  - Datum certificaat
  - Datum aanvang volgen cursus
  - Datum einde volgen cursus
  - Naam opleidingsinstituut

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen werkzoekende profiel vanaf overzicht geuploade werkzoekenden*
**Gegeven** de Gebruiker bekijkt de detail gegevens van een werkzoekende
**Wanneer** de Gebruiker de opleiding opvraagt
**Dan** krijgt de Gebruiker de opleiding van deze werkzoekende te zien

*Scenario: Raadplegen werkzoekende detailprofiel vanaf overzicht gevonden werkzoekenden*
**Gegeven** de Gebruiker bekijkt detail gegevens van een gevonde werkzoekenden  
**Wanneer** de Gebruiker de opleiding opvraagt
**Dan** krijgt de Gebruiker de opleiding van deze werkzoekende te zien
