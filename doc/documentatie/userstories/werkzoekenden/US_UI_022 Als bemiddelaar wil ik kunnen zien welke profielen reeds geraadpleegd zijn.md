# User Story : US_UI_022 Als bemiddelaar wil ik kunnen zien welke profielen reeds geraadpleegd zijn
versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** kunnen zien welke profielen reeds geraadpleegd zijn
**zodat ik** kan bepalen welke profielen geraadpleegd kunnen worden

### Functioneel
Een Bemiddelaar kan maar een beperkt aantal profielen raadplegen.
Om te kunnen beoordelen of een profiel bekeken moet worden is het noodzakelijk voor de bemiddelaar om te kunnen zien of een profiel al eerder bekeken is.

### User Stories
nvt

#### Afhankelijkheden

### Technische Documentatie
nvt

### High level Acceptatiecriteria

*Precondities :*
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Bemiddelaar bekijkt profiel voor de eerste keer*
**Gegeven** een profiel is niet bekeken 
**Wanneer** de bemiddelaar het profiel bekijkt 
**Dan** verschijnt een indicatie dat het profiel bekeken is

*Scenario: Bemiddelaar bekijkt profiel meerdere keren*
**Gegeven** een profiel is bekeken   
**Wanneer** de bemiddelaar het profiel bekijkt   
**Dan** blijft de indicatie dat het profiel bekeken is aanwezig
