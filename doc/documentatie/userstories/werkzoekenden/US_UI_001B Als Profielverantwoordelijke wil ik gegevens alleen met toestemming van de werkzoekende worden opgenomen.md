# User Story : Als Profielverantwoordelijke wil ik dat gegevens alleen met toestemming van de werkzoekende worden opgenomen zodat ik voldoe aan de AVG

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | december 2022 | Initiele opzet                        |


### Functioneel
Een voorwaarde om te kunnen werken met persoonsgegevens is de instemming van de werkzoekende. 
Hiertoe moet de gebruiker aangeven dat hiervoor toestemming is gevraagd en gegeven.  

### User Stories
nvt

#### Afhankelijkheden
Zie parent

### Technische Documentatie
nvt

### High level Acceptatiecriteria

*Precondities :*
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te uploaden

*Feature: Verlenen toestemming*
**Gegeven** de Gebruiker biedt een bestand werkzoekendeprofielen ter verwerking aan  
**Wanneer** de Gebruiker aangeeft dat er toestemming is verleend  
**Dan** kunnen de gegevens ge-upload worden 

*Scenario: geen toestemming*
**Gegeven** de Gebruiker biedt een bestand werkzoekendeprofielen ter verwerking aan  
**Wanneer** de Gebruiker aangeeft dat er toestemming is verleend  
**Dan** kunnen de gegevens niet ge-upload worden  