# User Story : US_UI_012C_Als Gebruiker wil ik de Werkervaring van een werkzoekendenprofiel kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** de Werkervaring van een werkzoekendenprofiel kunnen raadplegen
**zodat ik** ik kan beoordelen of de werkervaring aanlsuit op een vacature 

### Functioneel
Bij het matchen op een vacature is de werkervaring een van de belangrijkste criteria waarop de werkzoekende gevonden kan worden

- Interesse
- Werkervaring
  - Code beroepsnaam  
  - Omschrijving beroepsnaam  
  - Datum aanvang werkzaamheden
  - Datum einde werkzaamheden
  - Naam organisatie
  - Toelichting werkervaring
- Bemiddelingsberoep
  - Naam beroep
- Sector
  - Code SBI

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen werkzoekende profiel vanaf overzicht geuploade werkzoekenden*
**Gegeven** de Gebruiker bekijkt de detail gegevens van een werkzoekende
**Wanneer** de Gebruiker de werkervaring opvraagt
**Dan** krijgt de Gebruiker de werkervaring van deze werkzoekende te zien

*Scenario: Raadplegen werkzoekende detailprofiel vanaf overzicht gevonden werkzoekenden*
**Gegeven** de Gebruiker bekijkt detail gegevens van een gevonde werkzoekenden  
**Wanneer** de Gebruiker de werkervaring opvraagt
**Dan** krijgt de Gebruiker de werkervaring van deze werkzoekende te zien
