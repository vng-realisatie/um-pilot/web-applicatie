# User Story : US_UI_21 Als bemiddelaar wil ik kunnen zien hoeveel profielen bij een vraag nog opvraagbaar zijn

versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | februari 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** kunnen zien hoeveel profielen bij een vraag nog opvraagbaar zijn
**zodat ik** beter kan bepalen welke profielen ik opvraag 

### Functioneel
Om het aantal profielen per vraag dat bevraagd kan worden beperkt, is het noodzakelijk voor de bemiddelaar om inzicht te hebben in het aantal resterende aanvragen,

### User Stories
nvt

#### Afhankelijkheden

### Technische Documentatie
nvt

### High level Acceptatiecriteria

*Precondities :*
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Bemiddelaar bekijkt overzichten aanvragen per werkzoekende*
**Gegeven** er zijn een of meerdere aanvragen gedaan voor werkzoekendeprofielen
**Wanneer** de bemiddelaar het overzicht bekijkt 
**Dan** is per aanvraag het aantal resterende detailaanvragen beschikbaar
