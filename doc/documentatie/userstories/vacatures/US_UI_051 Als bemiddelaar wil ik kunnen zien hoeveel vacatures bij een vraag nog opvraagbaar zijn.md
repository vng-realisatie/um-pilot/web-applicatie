# User Story : US_UI_051 Als bemiddelaar wil ik kunnen zien hoeveel vacatures bij een vraag nog opvraagbaar zijn

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** kunnen zien hoeveel vacatures bij een vraag nog opvraagbaar zijn
**zodat ik** beter kan bepalen welke vacature ik opvraag 

### Functioneel
Om het aantal vacaturen per vraag dat bevraagd kan worden te beperken, is het noodzakelijk voor de bemiddelaar om inzicht te hebben in het aantal resterende aanvragen,

### User Stories
nvt

#### Afhankelijkheden

### Technische Documentatie
nvt

### High level Acceptatiecriteria

*Precondities :*
De Gebruiker is ingelogd en heeft rechten vacatures te raadplegen

*Feature: Bemiddelaar bekijkt overzichten aanvragen per vacature*
**Gegeven** er zijn een of meerdere aanvragen gedaan voor vacatures
**Wanneer** de bemiddelaar het overzicht bekijkt 
**Dan** is per aanvraag het aantal resterende detailaanvragen beschikbaar
