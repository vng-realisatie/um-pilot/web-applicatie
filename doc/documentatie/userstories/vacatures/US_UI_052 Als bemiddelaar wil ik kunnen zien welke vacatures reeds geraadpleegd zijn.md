# User Story : US_UI_052 Als bemiddelaar wil ik kunnen zien welke vacatures reeds geraadpleegd zijn

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** kunnen zien welke vacatures reeds geraadpleegd zijn
**zodat ik** kan bepalen welke vacatures geraadpleegd kunnen worden 

### Functioneel
Een Bemiddelaar kan maar een beperkt aantal vacatures raadplegen.
Om te kunnen beoordelen of een vacature bekeken moet worden is het noodzakelijk voor de bemiddelaar om te kunnen zien of een vacature al eerder bekeken is.

### User Stories
nvt

#### Afhankelijkheden

### Technische Documentatie
nvt

### High level Acceptatiecriteria

*Precondities :*
De Gebruiker is ingelogd en heeft rechten vacatures te raadplegen

*Feature: Bemiddelaar bekijkt profiel voor de eerste keer*
**Gegeven** een vacature is niet bekeken 
**Wanneer** de bemiddelaar de vacature bekijkt 
**Dan** verschijnt een indicatie dat de vacature bekeken is

*Scenario: Bemiddelaar bekijkt profiel meerdere keren*
**Gegeven** een vacature is bekeken   
**Wanneer** de bemiddelaar de vacature bekijkt   
**Dan** blijft de indicatie dat de vacature bekeken is aanwezig
