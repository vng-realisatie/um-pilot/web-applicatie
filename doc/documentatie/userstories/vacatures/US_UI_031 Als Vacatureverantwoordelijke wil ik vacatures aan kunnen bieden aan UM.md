# User Story : US_UI_031 Als Vacatureverantwoordelijke wil ik vacatures aan kunnen bieden aan UM
versie 0.10

_Versionering_

| versie | datum         | opmerking |
|--------|---------------|-----------|
| 0.10   | februari 2023 | Initieel  |


**Als** Als Vacatureverantwoordelijke  
**Wil ik** vacatures aan kunnen bieden aan UM
**zodat** deze vacatures vindbaar worden voor bemiddeling

Functioneel
-----------
Om matching mogelijk te maken moet een gebruiker in staat zijn om vacatures op te kunnen nemen in UM.
Een van de mogelijkheden die aangeboden wordt is het importeren van een json object met daarin de gegevens van de vacatures.
De gegevens worden batch gewijs opgevoerd. 
Deze oplossing biedt geen mogelijkheid tot het opvoeren van individuele vacatures.

Technisch
-------------
nvt

Acceptatiecriteria
------------------------
*Feature: upload valide batch vacatures*  
**Gegeven** een gebruiker is ingelogd  
**En** de gebruiker heeft rechten om vacatures te beheren   
**Wanneer** de gebruiker een valide json upload  
**En** de gebruiker het bestand indient  
**Dan** zijn de vacatures toegevoegd aan UM  
**En** wordt de gebruiker doorgeleid naar het overzicht geuploade vacatures  

*Feature: upload niet valide batch werkzoekenden*  
**Gegeven** een gebruiker is ingelogd  
**En** de gebruiker heeft rechten om vacatures te beheren  
**Wanneer** de gebruiker een niet valide json upload  
**En** de gebruiker het bestand indient  
**Dan** zijn de vacatures niet toegevoegd aan UM  
**En** zijn de bestaande vacatures niet verwijderd uit UM  
**En** krijgt de gebruiker een foutmelding  


