# User Story : US_UI_042C Als Gebruiker wil ik de gewenste Werkervaring van een vacature kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** gewenste Werkervaring van een vacature kunnen raadplegen
**zodat ik** ik kan beoordelen of de werkzoekende voldoet aan de gewenste werkervaring 

### Functioneel
Bij het matchen op een vacature is de werkervaring een van de belangrijkste criteria waarop beoordeeld kan worden of een werkzoekende voldoet aan de gestelde eisen

- Beroep
  - Code beroepsnaam
  - Omschrijving beroepsnaam
- Werkervaring
  - Aantal jaren werkervaring
- Sector
  - Code SBI

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten vacatures te raadplegen

*Feature: Raadplegen vacature vanaf overzicht algemene gegevens vacature*  
**Gegeven** de Gebruiker heeft de detail gegevens opgevraagd  
**Wanneer** de Gebruiker de werkervaring opgevraagt  
**Dan** krijgt de Gebruiker de gewenste werkervaring van deze vacature te zien  

