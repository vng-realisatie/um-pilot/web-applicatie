# User Story : US_UI_042B Als Gebruiker wil ik de Contactgegevens van een vacature kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** de Contactgegevens van een vacature kunnen raadplegen
**zodat ik** contact op kan nemen 

### Functioneel
In het geval van een match op een vacature kan via een van de contacgevens de vacature benaderd worden
Het gaat hierbij om :

- Werkgever
  - Handelsnaam organisatie
- Adreshouding  
  - Functie adres  
  - Datum aanvang adres
  - Datum einde adres
  - Code gemeente
  - District
  - Gemeentedeel
  - Gemeentenaam
  - Identificatie code nummeraanduiding
  - Identificatie code verblijfplaats
  - Locatieomschrijving
  - Postcode
  - Woonplaatsnaam
  - Antwoordnummer
- Contactpersoon
  - Naam contactpersoon
- Sector
  - Code SBI

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten vacatures te raadplegen

*Feature: Raadplegen vacature vanaf overzicht algemene gegevens vacature*
**Gegeven** de Gebruiker heeft de detail gegevens opgevraagd  
**Wanneer** de Gebruiker de contactgegevens opgevraagt  
**Dan** krijgt de Gebruiker de bekende contactgegevens van deze vacature te zien
