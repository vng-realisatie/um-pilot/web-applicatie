# User Story : US_UI_042D Als Gebruiker wil ik de gewenste Opleidingen voor een vacature kunnen raadplegen
versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** de gewenste Opleidingen voor een vacature kunnen raadplegen
**zodat ik** ik kan beoordelen of de opleiding van een werkzoekende aansluit op de vacature 

### Functioneel
Bij het matchen op een vacature is de opleiding een van de belangrijkste criteria waarop beoordeeld kan worden of de werkzoekende de juiste kwalificaties heeft

- Cursus
  - Naam cursus
- Opleiding
  - Niveau Opleiding  
  - Diploma behaald
  - Code opleidingsnaam 
  - Omschrijving opleiding
- Rijbewijs
  - Rijbewijs code

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen vacature vanaf overzicht algemene gegevens vacature*  
**Gegeven** de Gebruiker heeft de detail gegevens opgevraagd  
**Wanneer** de Gebruiker de werkervaring opgevraagt  
**Dan** krijgt de Gebruiker de gewenste opleiding van deze vacature te zien  
