# Feature : US_UI_053 Als bemiddelaar wil ik een vacature als een pdf kunnen downloaden

_Versionering_

2qkz4zc7s2

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.1    | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** een vacature als pdf kunnen downloaden  
**zodat** ik een offline vacature tot mijn beschikking heb

### Functioneel
Todo

#### Gerelateerde trello kaartjes ####
22-1 Als gemeente kan ik zowel de antwoorden als detailantwoorden downloaden in de UM GUI, om deze te exporteren uit het systeem.
22-2 Vormgeving exportbestand

### Technische Documentatie

### Acceptatiecriteria

*Dit zijn high-level acceptatie criteria die in de onderliggende User Stories verder worden uitgwerkt*

*Scenario : Downloaden pdf werkzoekendeprofiel*  
**Gegeven** de bemiddelaar raapleegt de vacature in detail 
**Wanneer** de bemiddelaar de vacature downloadt  
**Dan** wordt een pdf met daarop de vacature gegevens beschikbaar gesteld
