# User Story : US_UI_042A Als Gebruiker wil ik de algemene gegevens van een vacature kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** de algemene gegevens van een vacature kunnen raadplegen
**zodat ik** een algemeen beeld krijg van de vacature

### Functioneel
Tijdens het bemiddelen naar werk moeten bemiddelaars profielen kunnen beoordelen.
Hiertoe moeten zij een profiel kunnen aanwijzen en de detailgegevens bekijken.
In dit overzicht worden de algemene gegevens van het profiel getoond. 
Het gaat hierbij om :
- Algemene gegevens
  - ID vacature
  - Sluitingsdatum vacature
  - Code werk en denk niveau werkzoekende
  - Nummer vacature
  - Indicatie LDR registratie
  - Naam vacature
  - Omschrijving vacature
- Arbeidsvoorwaarden
  - Datum aanvang werkzaamheden
  - Datum einde werkzaamheden
  - Salaris indicatie
  - Omschrijving arbeidsvoorwaarden
- Flexibiliteit
  - code regiostraal
  - Datum aanvang beschikbaar voor werk
  - Datum einde beschikbaar voor werk
  - Indicatie onregelmatig werk of ploegendienst
- Werktijden
  - Aantal werkuren per week minimaal
  - Aantal werkuren per week maximaal
  - Indicatie kantoortijden
- Contractvorm
  - Type arbeidscontract
  - Type overeenkomst
- Sollicitatiewijze
  - Sollicitatiewijze
  - Code webadres
  - URL
- Vervoermiddel
  - Vervoermiddel
  - Vervoermiddel beschikbaar voor uitvoering werk
  - Vervoermiddel beschikbaar voor woon-werkverkeer

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen

*Feature: Raadplegen werkzoekende profiel vanaf overzicht geuploade werkzoekenden*
**Gegeven** de Gebruiker heeft een overzicht van werkzoekendenn  
**Wanneer** de Gebruiker de detail gegevens opvraagt
**Dan** krijgt de Gebruiker alle algemene gegevens over deze werkzoekende te zien

*Scenario: Raadplegen werkzoekende profiel vanaf overzicht gevonden werkzoekenden*
**Gegeven** de Gebruiker heeft een overzicht van gevonden werkzoekenden  
**Wanneer** de Gebruiker de detail gegevens van een werkzoekenden opvraagt
**Dan** krijgt de Gebruiker alle algemene gegevens over deze werkzoekende te zien
