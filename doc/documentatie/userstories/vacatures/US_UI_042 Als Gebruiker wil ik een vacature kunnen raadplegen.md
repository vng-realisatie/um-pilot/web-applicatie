# User Story : US_UI_042 Als Gebruiker wil ik een vacature kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** een vacature kunnen raadplegen
**zodat ik** de vacature kan beoordelen

### Functioneel
Tijdens het bemiddelen naar werk moeten bemiddelaars vacature kunnen beoordelen.
Hiertoe moeten zij een vacature kunnen aanwijzen en de detailgegevens kunnen bekijken.
In dit overzicht worden alle gegevens van de vacature getoond. 
Dit is een samenvoeging van de gegevens die beschreven zijn in de sub stories (zie hieronder)

### User Stories
De detailsgegevens zijn opgedeeld in een aantal secties die elk in een eigen userstory beschrven worden.
Het gaat hierbij om :
[Algemene gegevens](US_UI_042A%20Als%20Gebruiker%20wil%20ik%20de%20algemene%20gegevens%20van%20een%20vacature%20kunnen%20raadplegen.md)
[Contactgegevens](US_UI_042B%20Als%20Gebruiker%20wil%20ik%20de%20Contactgegevens%20van%20een%20vacature%20kunnen%20raadplegen.md)
[Werkervaring](US_UI_042C%20Als%20Gebruiker%20wil%20ik%20de%20gewenste%20Werkervaring%20van%20een%20vacature%20kunnen%20raadplegen.md)
[Opleiding](US_UI_042D%20Als%20Gebruiker%20wil%20ik%20de%20gewenste%20Opleidingen%20voor%20een%20vacature%20kunnen%20raadplegen.md)
[Kwalificatie](US_UI_042E%20Als%20Gebruiker%20wil%20ik%20de%20Kwalificaties%20voor%20een%20vacature%20kunnen%20raadplegen.md)

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :  
De Gebruiker is ingelogd en heeft rechten vacatures te raadplegen

*Feature: Raadplegen vacatures vanaf overzicht geuploade vacatures*  
**Gegeven** de Gebruiker heeft een overzicht van vacatures  
**Wanneer** de Gebruiker de detail gegevens opvraagt  
**En** het overzicht bekijkt  
**Dan** krijgt de Gebruiker alle bekende gegevens over deze vacature te zien

*Feature: Raadplegen vacatures vanaf overzicht opgeslagen zoekvraag vacatures*
**Gegeven** de Gebruiker heeft een overzicht van vacatures  
**Wanneer** de Gebruiker de detail gegevens opvraagt  
**En** het overzicht bekijkt  
**Dan** krijgt de Gebruiker alle bekende gegevens over deze vacature te zien  
