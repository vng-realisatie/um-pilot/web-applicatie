# User Story : US_UI_042E Als Gebruiker wil ik de Kwalificaties voor een vacature kunnen raadplegen

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** Kwalificaties van een vacature kunnen raadplegen
**zodat ik** ik kan beoordelen of de Kwalificaties aansluiten op een vacature 

### Functioneel
Bij het matchen op een vacature is de kwalificatie een van de criteria waarop een werkzoekende gematched kan worden

- Gedragscompetentie
  - Code gedragscompetentie
  - Beheersing gedragscompetentie
  - Omschrijving gedragscompetentie
- Vakvaardigheid
  - Omschrijving vakvaardigheid  
- Taalbeheersing
  - Code taal
  - Taalbeheersing mondeling
  - Taalbeheersing schriftelijk
  - Taalbeheersing lezen
  - Taalbeheersing luisteren  
  - 
### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :
De Gebruiker is ingelogd en heeft rechten werkzoekendeprofielen te raadplegen
*Feature: Raadplegen vacature vanaf overzicht algemene gegevens vacature*  
**Gegeven** de Gebruiker heeft de detail gegevens opgevraagd  
**Wanneer** de Gebruiker de kwalificaties opgevraagt  
**Dan** krijgt de Gebruiker de gewenste kwalificaties van deze vacature te zien  
