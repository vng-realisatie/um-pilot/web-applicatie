# User Story : US_UI_041 Als bemiddelaar wil ik vacatures kunnen opvragen

versie 0.10

_Versionering_

| versie | datum      | opmerking                             |
|--------|------------|---------------------------------------|
| 0.10   | maart 2023 | Initiele opzet                        |

**Als** bemiddelaar  
**wil ik** vacatures kunnen opvragen
**zodat ik** vacature kan vinden passend bij een profiel

### Functioneel
Tijdens het bemiddelen naar werk moeten bemiddelaars vacatures op basis van kenmerken kunnen vinden.
Deze functionaliteit filtert de vacatures op basis van geregistreerde gegevens bij deze vacatures.
De gegevens waarop gefilterd kan worden zijn :
- Algemene gegevens
  - Indicatie LDR registratie
  - Code werk en denk niveau werkzoekende
  - Sluitingsdatum vacature
- Arbeidsvoorwaarden
  - Datum aanvang werkzaamheden
  - Datum einde werkzaamheden
  - Salaris indicatie
- Flexibiliteit
  - code regiostraal
  - Datum aanvang beschikbaar voor werk
  - Datum einde beschikbaar voor werk
  - Indicatie onregelmatig werk of ploegendienst
- Werktijden
  - Aantal werkuren per week minimaal
  - Aantal werkuren per week maximaal
  - Indicatie kantoortijden
- Beroep
  - Type beroepsnaam
  - Code beroepsnaam / Omschrijving beroepsnaam
  - Naam beroep
- Sector
  - Code SBI
- Contractvorm
  - Type arbeidscontract
  - Type overeenkomst
- Sollicitatiewijze
  - Sollicitatiewijze
- Werkervaring
  - Aantal jaren werkervaring
- Vervoermiddel
  - Vervoermiddel beschikbaar voor uitvoering werk
  - Vervoermiddel beschikbaar voor woon-werkverkeer
- Cursus
  - Naam cursus
- Opleiding
  - Niveau Opleiding
  - Diploma behaald
  - Type opleidingsnaam
  - Code opleidingsnaam
  - Omschrijving opleiding
- Gedragscompetentie
  - Gedragscompetentie
  - Beheersing gedragscompetentie
  - Omschrijving gedragscompetentie
- Vakvaardigheid
  - Omschrijving vakvaardigheid
- Taalbeheersing
  - Code taal
  - Taalbeheersing mondeling
  - Taalbeheersing schriftelijk
  - Taalbeheersing lezen
  - Taalbeheersing luisteren
- Rijbewijs
  - Rijbewijs code

Wanneer er gefilterd worden alleen die vacatures aangeboden die voldoen aan de opgegeven criteria.
Wanneer er van een bepaalde categorie meerdere worden opgegeven dan geldt dat de vacature aan een van deze criteria dient te voldoen ('of')
Wanneer er gefilterd wordt op een combinatie van criteria dan dienen de aangeboden vacatures te voldoen aan de combinatie van die criteria.
Maw dit is een 'En'-relatie en geen 'Of'.

### User Stories
nvt

### Technische Documentatie
nvt

### High level Acceptatiecriteria

#### Precondities :
De Gebruiker is ingelogd en heeft rechten vacatures te raadplegen

*Feature: Indienen verzoek voor vacatures*
**Gegeven** de Gebruiker geeft een zoektitel en minimaal een ander criterium op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die aan de gestelde criteria voldoen  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen ongeldig verzoek*  
**Gegeven** de Gebruiker geeft geen zoektitel  
**Of** geen ander criterium op  
**Wanneer** de Gebruiker het verzoek wil indienen  
**Dan** krijgt de Gebruiker niet de mogelijkheid een verzoek in te dienen  

*Scenario: Indienen verzoek voor vacatures met Indicatie LDR registratie*  
**Gegeven** de Gebruiker geeft een zoektitel op   
**En** de Gebruiker geeft een 'Indicatie LDR registratie' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan de 'Indicatie LDR registratie'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor vacatures met 'Code werk en denk niveau'*   
**Gegeven** de Gebruiker geeft een zoektitel op   
**En** de Gebruiker geeft 'Code werk en denk niveau werkzoekende' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan de 'Code werk en denk niveau werkzoekende'   
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor vacatures met 'Sluitingsdatum vacature'*   
**Gegeven** de Gebruiker geeft een zoektitel op   
**En** de Gebruiker geeft een 'Sluitingsdatum vacature' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen liggen voor de 'Sluitingsdatum vacature'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Arbeidsvoorwaarden - Datum aanvang werkzaamheden'*   
**Gegeven** de Gebruiker geeft een zoektitel op   
**En** de Gebruiker geeft een 'Datum aanvang werkzaamheden' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen liggen op of na de 'Datum aanvang werkzaamheden'    
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor vacatures met 'Arbeidsvoorwaarden - Datum einde werkzaamheden'*  
**Gegeven** de Gebruiker geeft een zoektitel op   
**En** de Gebruiker geeft een 'Datum aanvang werkzaamheden' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen liggen voor de 'Datum einde werkzaamheden'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor vacatures met 'Arbeidsvoorwaarden - Salaris indicatie'*  
**Gegeven** de Gebruiker geeft een zoektitel op
**En** de Gebruiker geeft een 'Datum aanvang werkzaamheden' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan (>=) de 'Salaris indicatie'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met Flexibiliteit*
**Gegeven** de Gebruiker geeft een zoektitel op
**En** de Gebruiker geeft 'Flexibiliteit' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan de 'Flexibiliteit'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met Werktijden*
**Gegeven** de Gebruiker geeft een zoektitel op
**En** de Gebruiker geeft 'Werktijden' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan de 'Werktijden'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Beroep'*  
**Gegeven** de Gebruiker geeft een zoektitel op  
**En** de Gebruiker geeft een 'Beroep' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan het 'Beroep'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina  

*Scenario: Indienen verzoek voor vacatures met 'Sector'*  
**Gegeven** de Gebruiker geeft een zoektitel op  
**En** de Gebruiker geeft een 'Sector' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan de 'Sector'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Contractvorm'*  
**Gegeven** de Gebruiker geeft een zoektitel op  
**En** de Gebruiker geeft een of meerdere 'Contractvormen' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Contractvormen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met Sollicitatiewijze*  
**Gegeven** de Gebruiker geeft een zoektitel op
**En** de Gebruiker geeft een of meerdere 'Sollicitatiewijzes' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Sollicitatiewijzes'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Werkervaring'*  
**Gegeven** de Gebruiker geeft een zoektitel op  
**En** de Gebruiker geeft een of meer jaren 'Werkervaring' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Werkervaringen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Beschikbaarheid Vervoermiddel'*  
**Gegeven** de Gebruiker geeft een zoektitel op  
**En** de Gebruiker geeft een of meerdere 'Beschikbaarheden Vervoermiddel' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Beschikbaarheden Vervoermiddel'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Cursus'*  
**Gegeven** de Gebruiker geeft een zoektitel op  
**En** de Gebruiker geeft een of meerdere 'Cursussen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Cursussen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Opleidingen'*  
**Gegeven** de Gebruiker geeft een zoektitel op  
**En** de Gebruiker geeft een of meerdere 'Opleidingen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Opleidingen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Gedragscompetentie'*  
**Gegeven** de Gebruiker geeft een zoektitel op
**En** de Gebruiker geeft een of meerdere 'Competenties' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Competenties'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Vakvaardigheid'*  
**Gegeven** de Gebruiker geeft een zoektitel op
**En** de Gebruiker geeft een of meerdere 'Vakvaardigheden' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Vakvaardigheden'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Taalbeheersing'*  
**Gegeven** de Gebruiker geeft een zoektitel op   
**En** de Gebruiker geeft een of meerdere 'Taalbeheersingen' op   
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Taalbeheersingen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina

*Scenario: Indienen verzoek voor vacatures met 'Rijbewijs'*
**Gegeven** de Gebruiker geeft een zoektitel op
**En** de Gebruiker geeft een of meerdere 'Rijbewijzen' op  
**Wanneer** de Gebruiker het verzoek indient  
**Dan** krijgt de Gebruiker als resultaat maximaal 10 resultaten van vacatures die voldoen aan een van de 'Rijbewijzen'  
**En** wordt de gebruiker omgeleid naar de resultaten pagina
