# User Story : US_UI_100 Als VNG wil ik dat de UI aan de huisstijl voldoet

_Versionering_

| versie | datum        | opmerking   |
|--------|--------------|-------------|
| 0.10   | januari 2023 | onderhanden |

**Als** VNG
**wil ik** dat de VUM-UI aan de huisstijl voldoet
**zodat**  de web-applicaties er uniform uitzien

Functioneel
----------------
Verzamel issue voor eerder geconstateerde issues

Trello tickets :
- 10-9-3 UM portaal = UM VUM Portaal (er komen meerdere UM toepassingen)
- 10-9-6 Engelse termen vervangen naar NL
- 10-9-9 Overzichtspagina upload werkzoekende/vacatures - tabel tonen bemiddelingsberoepPortaal - Tekst Startpagina
- 10-9-10 Onduidelijk wat Voorkeursland betekent
- Informatiebubbel met meer info bij velden
- Toevoegen Rijbewijs gelijktrekken met andere items (+)
- Front end feedback
- Lettertype aanpassen naar Monserrat (VNG huisstijl)
- Embleem UM logo rechtsonderin de footer
- Kleuren navigatie/footer in grijstinten met contrast.
- UM logo onder de logout button
- inlogscherm achtergrond aanpassen
- Footer overflow in body
- De tekst "UM VUM Portaal" op de hoofd- en inlogpagina veranderen in "UM-w Portaal"
- US_FO_001 Uniforme form lay-out
- US_FO_002_WZP header Code werk- en denkniveau

- De tekst is "Het unieke nummer waarmee een profiel wordt geïdentificeerd in een bronsysteem" maar kan beter zoiets zijn als.
"Een uniek kenmerk van de vraag waarop je deze vraag eenvoudig in het overzicht kunt terugvinden."
De tekst is "Het unieke nummer waarmee een profiel wordt geïdentificeerd in een bronsysteem" maar kan beter zoiets zijn als.
"Een uniek kenmerk van de vraag waarop je deze vraag eenvoudig in het overzicht kunt terugvinden."


Bij het uploaden van een vacaturebestand komt alles goed door op API niveau, maar frontend toont max. maar twee resultaten.

Gebruikersrollen aanmaken in Keycloak
- rapportage (alleen rapportage)
- upload werkzoekende
- zoeken vacatures
- zoeken werkzoekende
- upload vacatures
- superuser: is alle rollen bij elkaar.
- Nog een user boven superuser (beheerder van UM) die nieuwe gebruikers in Keycloak kan aanmaken.
