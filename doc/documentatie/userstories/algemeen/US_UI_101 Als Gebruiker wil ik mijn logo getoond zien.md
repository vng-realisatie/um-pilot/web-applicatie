# User Story : US_UI_101 Als Gebruiker wil ik mijn logo getoond zien
versie 0.10

_Versionering_

| versie | datum         | opmerking                             |
|--------|---------------|---------------------------------------|
| 0.10   | december 2022 | Initiele opzet                        |

**Als** Gebruiker
**Wil ik** mijn logo getoond zien
**zodat** de bemiddelaar weet voor welke instantie wordt bemiddelt


Functioneel
----------------
Voor een gebruiker (geidentificeerd middels het OIN) dient linksboven in het scherm het logo van die instantie getoond te worden

Technisch
-------------
nvt

Acceptatiecriteria
------------------------
*Feature:*
**Als** Bemiddelaar
**Wil ik** het logo van mijn organisatie in de menubalk zien
**zodat** duidelijk is voor welke instantie wordt bemiddeld

*Background:*
**Gegeven** een organisatie ""UWV"
**En** voor deze organisatie is een logo beschikbaar
**En** een organisatie "Gemeente Arnhem"
**En** voor deze organisatie is geen logo beschikbaar

*Scenario: Logo is beschikbaar*
**Gegeven** een bemiddelaar van organisatie "UWV"
**Wanneer** deze bemiddelaar inlogt
**Dan** wordt het logo van de de organisatie "UWV" linksboven getoond

*Scenario: Logo is niet beschikbaar*
**Gegeven** een bemiddelaar van organisatie "Gemeente Arnhem"
**Wanneer** deze bemiddelaar inlogt
**Dan** wordt het logo van VNG linksboven getoond
