# Feature : US_UI_001 Als gebruiker wil ik toegang tot de UM GUI

versie 0.10

_Versionering_

| versie | datum         | opmerking |
|--------|---------------|-----------|
| 0.10   | december 2022 | TODO      |
| 0.10   | februari 2023 | Afgerond  |


**Als** Gebruiker
**Wil ik** toegang tot de UM GUI
**zodat** ik mijn beheerswerkzaamheden uit kan voeren


Functioneel
----------------
Een gebruiker moet in kunnen loggen zodat deze gebruik kan maken van de handmatige invoer mogelijkheden en bestandsupload

zie 10.4 in https://vvng.sharepoint.com/:w:/r/sites/UM-UitwisselingsMechanisme/Gedeelde%20documenten/Techniek/Documentatie%20UM/User%20stories%20voor%20bestaande%20bouwstenen%20UM%20MVP%20.docx?d=wf60a0fbb11d24b148fce24c4acedf9a4&csf=1&web=1&e=ekg5To

Technisch
-------------
Voor de pilot volstaat vooralsnog het inloggen middels username en wachtwoord?
E-herkenning moet? nog geimplementeerd worden. Dit wordt dan een aparte user story

Acceptatiecriteria
------------------------
*Feature:*
**Gegeven** een gebruiker is niet ingelogd
**Wanneer** de gebruiker inlogt met valide credentials
**Dan** heeft de gebruiker toegang tot het beheer van de UM GUI

**Gegeven** een gebruiker is niet ingelogd
**Wanneer** de gebruiker inlogt
**En** de gebruiker geeft niet valide valide credentials
**Dan** heeft de gebruiker geen toegang tot de UM GUI
