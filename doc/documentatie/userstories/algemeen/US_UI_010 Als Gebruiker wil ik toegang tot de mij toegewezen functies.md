# User Story : US_UI_002 Als Gebruiker wil ik toegang tot de mij toegewezen functies

_Versionering_

| versie | datum        | opmerking   |
|--------|--------------|-------------|
| 0.10   | januari 2023 | onderhanden |
| 0.10   | maart 2023   | afgerond    |

**Als** VNG
**wil ik** toegang tot de mij toegewezen functies
**zodat** ik mijn werkzaamheden kan verrichten

Functioneel
----------------
Voor een gebruiker (geidentificeerd middels het OIN) dient linksboven in het scherm het logo van die instantie getoond te worden

Technisch
-------------
nvt

Acceptatiecriteria
------------------------
*Feature:*
**Gegeven** een gebruiker is ingelogd
**Wanneer** de gebruiker zich op een willekeurige pagina bevindt
**Dan** ziet de gebruiker alleen de menu-items waartoe deze geauthoriseerd is

Backround:
**Gegeven** een rol "Werkzoekendenbeheerder" die rechten geeft om werkzoekenden te beheren
**En** een rol "Vacaturebeheerder" die rechten geeft om vacatures te beheren
**En** een gebruiker Jan zonder rollen
**En** een gebruiker Farida met de rol "Werkzoekendenbeheerder"
**En** een gebruiker Monica met de rol "Vacaturebeheerder"
**En** een Gebruiker Gerard met de rollen:
| Werkzoekendenbeheerder |
| Vacaturebeheerder |

*Scenario: Geen autorisatie*
**Wanneer** Jan inlogt
**Dan** worden de volgende 2 menu items getoond:
| Startpagina |
| Uitloggen |

*Scenario: Autorisatie tot werkzoekenden*
**Wanneer** Farida inlogt
**Dan** worden de volgende 6 menu items getoond:
| Startpagina |
| Upload werkzoekenden |
| Overzicht geüploade werkzoekenden |
| Vraag werkzoekende aan |
| Opgeslagen zoekvraag werkzoekenden |
| Uitloggen |

*Scenario: Autorisatie tot vacatures*
**Wanneer** Monica inlogt
**Dan** worden de volgende 6 menu items getoond:
| Startpagina |
| Upload vacatures |
| Overzicht geüploade vacatures |
| Vraag vacatures aan |
| Opgeslagen zoekvraag vacatures |
| Uitloggen |

*Scenario: Autorisatie tot werkzoekenden en vacatures*
**Wanneer** Gerard inlogt
**Dan** worden de volgende 10 menu items getoond:
| Startpagina |
| Upload vacatures |
| Overzicht geüploade vacatures |
| Vraag vacatures aan |
| Opgeslagen zoekvraag vacatures |
| Upload werkzoekenden |
| Overzicht geüploade werkzoekenden |
| Vraag werkzoekende aan |
| Opgeslagen zoekvraag werkzoekenden |
| Uitloggen |
